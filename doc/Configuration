The Configuration File
----------------------

bwm_tools is based on a xml configuration file which has the following
format...


<firewall>
	<global>
		<modules>
			<load name="..."/>
			<load name="..."/>
			<load name="..."/>
		</modules>
		.
		.
		.
		<class name="......">
			<address name="..."/>
			.
			.
			.
		</class>
	.
	.
	.
	</global>
	<acl>
		<table name="...">
			<chain name="...">
				<rule>
					...class name here...
					.
					.
				</rule>
			</chain>
			.
			.
			.
		</table>
	</acl>
	<nat>
		<snat>
			<rule to-src="...">
			.
			.
			.
			</rule>
		</snat>
		<dnat>
			<rule to-dst="...">
			.
			.
			.
			</rule>
		</dnat>
	</nat>
	<traffic>
		<flow name="...">
			<flow name="...">
			.
			.
			.
				.
				.
				.
			.
			.
			.
			</flow>
		</flow>
		<group name="...">
			.
			.
			.
		</group>
	</traffic>
.
.
.
</firewall>


Quick explanation:
------------------

The main firewall tag defines this file is infact a firewall and is
the root tag looked for when parsing takes place.

The global tag defines global parameters & classes, classes are used to 
define traffic. Classes have to have names as this is how they are 
referenced. A class can contain multiple addresses. Addresses can have 
names and might be used in future. One kind of parameter is the modules,
modules can be specified to load before bwmd starts... the most common
use for this is to load ip_queue and maybe some conntrack stuff, like ftp.

The ACL section contains rules which define access control. Table tags
are defined which contain chains, chains can have default targets.
Chains then contain rules and rules contain class names. All class names
appearing between the rule tags are affected by the specific rule. Tables 
and rules must have names and rules have to have targets. Rules can have
names and might be used in future.

The NAT section defines the network address translation list. There is both
tags for the SNAT & DNAT. These both contain rules which have the same basic
format as the ACL section, here though instead of the target "to-src" or
"to-dst" must be specified respectively. Rules again can have names that
might be used in future.

The TRAFFIC section contains both information used for logging & bandwidth 
control. Flows must have a name as this is used for the log filename. Flows
can be embedded aswell, this allows extreme control over bandwidth shaping.
Between PARENT flow tags classes cannot be specified, between end (child)
flow tags classes are specified along with a nfmark which creates a firewall
mark and matches the classes specified. Queues can be used to classify traffic
into priorities. The group tags allow the adding up of the flow names which 
appear between the tags, therefore you can have all your mailservers in a 
group which has a specific name, and also in another group if they part of 
different subnets.


More detailed section explanation:
----------------------------------

<global></global>:
	This tag takes no parameters

	<modules></modules>:
		This tag takes no parameters

		<load></load>:
			name=""
				- Name of module to load
			params=""
				- Modprobe parameters after module name
				  (module parameters)

	<class></class>:
		name="" 
			- This specifies the classes name

		<address></address>:
			name=""
				- This is optional and contains the
				    name of the address specified
			dst=""    
				- Destination IP
			dst-iface=""
				- Destination interface
			dst-port=""
				- Destination port
			proto=""
				- Protocol
			src=""
				- Source IP
			src-iface=""
				- Source interface
			src-port=""
				- Source port
	
<acl></acl>:
	These tags take no parameters
	
	<table></table>:
		name=""
			- This specifies the table name

		<chain></chain>:
			name=""
				- This specifies the chain name
			default=""
				- This is used to set the default policy for
				  the standard chains

			<rule></rule>:
				name=""
					- This is optional and sets the rule
					  name
				cmd-line=""
					- This is extra command line parameters
					  given to iptables when generating the
					  the firewall
				target=""
					- Rules need a target

<nat></nat>:
	These tags take no parameters

	<snat></snat>:
		These tags take no parameters

		<rule></rule>:
			name=""
				- This is optional, specifies the name of the
				  rule
			to-src=""
				- Set the source address of the classes 
				  specified
	<dnat></dnat>:
		These tags take no parameters

		<rule></rule>:
			name=""
				- This is optional, specifies the name of the
				  rule
			to-dst=""
				- Set the destination address of the classes 
				  specified

<traffic></traffic>:
	The tags take no parameters

	<flow></flow>:
		name=""
			- This tag specifies the name of the flow, and is used
			  to determine the log file name
		stats-len=""
			- This sets the period in seconds that the average
			  bandwidth rate is based on. If 0 is specified there 
		          will be no average
		queue-size=""
			- Maximum size of the packet queue in bytes, if
			  exceeded packets will be dropped
		queue-len=""
			- Maximum number of packets to be queued before
			  packets are dropped
		max-rate=""
			- Maximum rate in bytes/s before packets are queued,
			  packets are not queued if they can be bursted. If
			  0 is specified only logging of traffic will occur
		burst-rate=""
			- Maximum rate in bytes/s which packets can be
			  bursted. Packets can be bursted until the average
			  rate exceeds the max-rate. Unlimited bursting will
			  occur when burst-rate = 0
		report-timeout=""
			- How many seconds elapse before flow details are
			  logged to file
		nfmark=""
			- This must only be specified along with class names
			  between tags, as this will allow actual traffic to
			  be matched. This is normally used at the deepest
			  level of flow embedding
		
		<queue></queue>:
			prio=""
				- Priority of the queue
			nfmark=""
				- As above, allows traffic to be matched. Queues
				  nfmark value takes priority over the flow's.

	<group></group>:
		name=""
			- This tag specifies the name of the flow group, and is used
			  to determine the log file name. The group contains flow names
			  one per line between tags.
		stats-len=""
			- This sets the period in seconds that the average bandwidth
			  rate is based on. If 0 is specified there will be no 
			  average.
		report-timeout=""
			- How many seconds elapse before group details are logged to 
			  file



Limitations:
------------

* Flow names cannot contain funny characters, please
  stick to  a-z, including 0-9 and _.

* Hash (#) based comments don't work when commenting out XML

