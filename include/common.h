/*
 * 	common.h - Header stuff commonly used by more than 1 .c file
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  15/04/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/



#ifndef _COMMON_H
#define _COMMON_H

#include <glib.h>
#include <linux/limits.h>
#include "libipq.h"


#define BUFFER_SIZE 4096

#define MAX_REFLEN 128

#define MAX_NAMELEN PATH_MAX

#define NUM_PRIO_BANDS	100

#define malloc0(xxxx) memset(malloc(xxxx),'\0',xxxx)


// Free memory list
struct pmem_t
{
	GList *freeList;
	GMutex *lock;
};

// Thread data
struct runnerData_t
{
	int daemon; // This is set to 1 if we are a daemon, and 0 if not
	
	GList *pktQueues; // List of our packet queues
	GList *flows; // List of our flows
	GList *groups; // List of our groups
	
	GCond *bandSignalCond;  // Signal used to tell us a band has changed
	GMutex *bandSignalLock;
	int queueChanged;
	GList *queueChangeList[NUM_PRIO_BANDS];  // List of bands changed
	GMutex *waitListLock;
	int queueWait;
	GList *queueWaitList[NUM_PRIO_BANDS];  // List of bands waiting
	
	struct ipq_handle *IPQHandle;  // ip_queue handle
	GMutex *IPQLock;
	
	struct pmem_t pmem;  // List of free memory we can use
};

// Flow data
struct flowData_t
{
	GList *pktQueues;
	GList *flows;
	GList *groups;
};


// This is used in the queueing and is the item used in the queue
struct packet_t
{
	ipq_packet_msg_t *payload;
	unsigned char changed;
};



#endif

