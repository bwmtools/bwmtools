/*
 * 	flow.h - Headerfile for bwmd
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  15/04/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/


#ifndef _FLOW_H
#define _FLOW_H


#include <glib.h>
#include <netinet/in.h>
#include <linux/netfilter.h>
#include <sys/time.h>
#include <syslog.h>
#include "common.h"
#include "ipq.h"
#include "libipq.h"


// Parent flow member,  yyyy = member
#define P_FLOW(pktQueue,yyyy) (pktQueue->parentFlow)->yyyy

// Packet payload length
#define PKT_SIZE(packet) packet->payload->data_len
// Packet id
#define PKT_ID(packet) packet->payload->packet_id


// Our packet statistic structure
struct pktStat_t
{
	unsigned int pktCount;
	unsigned int pktSize;
	unsigned int pktDropped;
	unsigned int pktBursted;
};


// A packet queue
struct pktQueue_t
{
	GMutex *lock;

	unsigned char prio;
	
	struct flow_t *parentFlow;  // void* because we cannot ref below?

	unsigned long int nfmark;
	
	long int curSize;
	long int curLen;
	long int maxSize;
	long int maxLen;
	
	GList *packets;
};


// A flow
struct flow_t
{
	// Lock is used to lock basically everything, cept counter
	GMutex *lock;

	// Flow identifier
	char flowName[MAX_REFLEN];  // STATIC - my reference

	// Stats stiff
	long int statsLen;  // STATIC - max length of stat array
	long int statsPos;
	struct pktStat_t *pktStats;  // uses ++ to increase in temp var to get to statsLen

	// Shaping	
	struct flow_t *parent;  // STATIC, not specified, determined - pointer to parent
	unsigned char prioClassifier;  // STATIC - Flow priority auto classifier
	unsigned long int nfmark;  // STATIC - nfmark value i must match auto to queues
	long int maxQueueSize;  // STATIC - max length in bytes of queue
	long int maxQueueLen;  // STATIC - and/or length in items
	long int maxRate;  // STATIC - max rate in bytes per second
	long int burstRate;  // STATIC - max rate bytes per second
	long int curQueueSize;  // Current queue length in bytes, this is included in the runningLock
	long int curQueueLen;  // Current queue length in items, this is included in the runningLock
	struct pktQueue_t *pktQueues[NUM_PRIO_BANDS];

	// Setup our thresholds
//	long int min_th;  // We start dropping packets here
//	long int max_th;  // We drop alot more here
	float parent_th;  // Used to check the available percentage of parent bandwidth before we burst
	
	// Counters, used for both shape & counter logging
	GMutex *counterLock;  // Lock used only for locking these counters
	struct pktStat_t counter;  // This is the counter which is logged to file
	GList *groups;

	// Credit stuff... this is used in calculating current credit
	double usCredit;  // Credits for maxRate per microsecond
	long int curCredit;  // Current available credit for maxRate, if we burst we will be < 0
	double usBurstCredit;
	long int curBurstCredit;  // Curent burstable credit available

	// Throughput and internal stats
	unsigned int curThroughputAge; // How many microseconds since last throughput update
	struct timeval lastThroughputUpdate;  // Last time the throughput was updated
	unsigned int accumThroughput; // Accumulated throughput
	unsigned int accumPackets; // Accumulated packets
	float curThroughput; // Current throughput
	unsigned int curPacketRate; // Current throughput
	unsigned int softQueueSize;  // Queue size
	
	unsigned int accumMs;  // Accumulated number of microseconds
	struct timeval lastCreditCalc;  // Last "timeval" that we calculated additional credit
	struct pktStat_t running;  // This is the running counter used for stats & logging

	// Reporting stuff	
	int lastDumpTimestamp;
	int counterTimeout;  // STATIC - length between file logs
	int counterRemaining;
	int reportFormat;
	char *reportFilename;
};


// A group
struct group_t
{
	// Lock is used to lock basically everything, cept counter
	GMutex *lock;

	// Group identifier
	char groupName[MAX_REFLEN];

	// Stats stiff
	long int statsLen;  // STATIC - max length of stat array
	long int statsPos;
	struct pktStat_t *pktStats;  // uses ++ to increase in temp var to get to statsLen

	// Counters
	GMutex *counterLock;  // Lock used only for locking these counters
	struct pktStat_t counter;
	struct pktStat_t running;

	int lastDumpTimestamp;
	int counterTimeout;  // STATIC - length between file logs
	int counterRemaining;
	int reportFormat;
	char *reportFilename;

	// Flows we "contain"
	GList *flowList;
};


// Get avg flow stats
struct pktStat_t getFlowTotalStats(struct pktStat_t *pktStats, long int pktStatsLen);


// Function to update all our flow groups
void updateGroups(
		struct flow_t *flow, 
		int pktCount, 
		int pktSize, 
		int pktDropped, 
		int pktBursted);


// Message log facility
void logMessage(int priority, const char *fmt, ...);


// Flow runner thread
void *flowRunner(void *data);


// Check if we will exceed flow queuing limits
static inline int will_exceed_flow_queue(struct flow_t *flow, unsigned int pktSize)
{
	return ((flow->curQueueLen >= flow->maxQueueLen && flow->maxQueueLen != 0) ||
			(flow->curQueueSize + pktSize >= flow->maxQueueSize && flow->maxQueueSize != 0));
}

#endif

