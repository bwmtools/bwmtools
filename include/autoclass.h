/*
 * 	autoclass.h - Automatic packet priority classification header file
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  29/08/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/

#ifndef _AUTOCLASS_H
#define _AUTOCLASS_H


#include <stdlib.h>
#include "flow.h"


// Classificaiton types...
#define AUTOCLASS_NONE  0
#define AUTOCLASS_PORT	1
#define AUTOCLASS_TOS	2


// Type of Service / DSCP / ECN ... etc
#define AUTOCLASS_TOS_ECN_MASK(tos)		((tos) & 0x03)  // Mask of all ECN bits
#define AUTOCLASS_TOS_ECN_ECT0			0x01
#define AUTOCLASS_TOS_ECN_ECT1			0x02
#define AUTOCLASS_TOS_ECN_CE			0x03

#define AUTOCLASS_TOS_MASK(tos)			((tos) & 0x1c)  // Mask of all TOS bits
#define AUTOCLASS_TOS_RELIABILITY		0x04  // High Reliability
#define AUTOCLASS_TOS_THROUGHPUT		0x08  // High Throughput
#define AUTOCLASS_TOS_LOWDELAY			0x10  // Low Delay

#define AUTOCLASS_TOS_PREC_MASK(tos)	((tos) & 0xe0)  // Mask of all PREC bits
#define AUTOCLASS_TOS_PREC_ROUTINE		0x00  // Routine
#define AUTOCLASS_TOS_PREC_PRIORITY		0x20  // Priority
#define AUTOCLASS_TOS_PREC_IMMEDIATE	0x40  // Immediate
#define AUTOCLASS_TOS_PREC_FLASH		0x60  // Flash
#define AUTOCLASS_TOS_PREC_FLASH_OVERRIDE 0x80  // Flash Override
#define AUTOCLASS_TOS_PREC_CRITIC_ECP	0xa0  // Critic/ECP
#define AUTOCLASS_TOS_PREC_INTERNETWORK_CONTROL 0xc0  // Internetwork control
#define AUTOCLASS_TOS_PREC_NETWORK_CONTROL 0xe0  // Network control


// Auto classify packet and return priority (1 - best, 100 - worst)
unsigned char autoClassify(struct ip_packet_t *ip_packet, unsigned char prioClassifier);


#endif
