/*
 * 	flowControl.h - Control interface stuff
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  02/06/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/





#ifndef _FLOWCONTROL_H
#define _FLOWCONTROL_H

#include "common.h"
#include "flow.h"


#define SEC_ELAPSED(now,before,seconds) \
	(now.tv_sec - before.tv_sec >= seconds)

#define USEC_ELAPSED(now,before,usecs) \
	(now.tv_sec - before.tv_sec >= 1 || now.tv_usec - before.tv_usec >= usecs)


// Packet types
enum {
	IPC_PACKET_PING,
	IPC_PACKET_PONG,
	
	IPC_PACKET_ADD,
	IPC_PACKET_DEL,
	
	IPC_PACKET_FLOW_DATA,
	
	IPC_PACKET_LIST_FLOWS,
	IPC_PACKET_LIST_ITEM,
	IPC_PACKET_LIST_END,
	
	IPC_PACKET_GET_NUM_FLOWS,
	IPC_PACKET_NUM_FLOWS,
		
	IPC_PACKET_ERROR
};


// Ping specific packet
struct ping_packet_t
{
	time_t timestamp;
};


// Status specific packet
struct status_packet_t
{
	char flowName[MAX_REFLEN];
	long int maxQueueSize;
	long int maxQueueLen;
	long int maxRate;
	long int burstRate;
	struct pktStat_t running;
	struct pktStat_t avg;
	float queueItemUsage;
	float queueSizeUsage;
};

// This is a typical packet sent from server to client, or visa versa
struct ipc_packet_t
{
	int pktType;
	union {
		struct ping_packet_t pingData;
		struct status_packet_t statusData;
		int num;
	} u;
};


// This is a client connection
struct ipc_client_t
{
	int fd;
	GList *flowStatusList;
	GList *groupStatusList;
};



// Control thread
void *controlRunner(void *data);

#endif

