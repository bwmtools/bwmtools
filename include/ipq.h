/*
 * 	ipq.h - IPQ header file for bwmd
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  15/04/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/


#ifndef _IPQ_H
#define _IPQ_H

#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <glib.h>
#include "common.h"
#include "libipq.h"


#define IPQ_BUFSIZE 2048

struct addr_t
{
	u_int8_t a;
	u_int8_t b;
	u_int8_t c;
	u_int8_t d;
};

// Hijacked out of /usr/include/netinet/ip.h
struct ip_packet_t
{
#if __BYTE_ORDER == __LITTLE_ENDIAN
	unsigned int ihl:4;
	unsigned int version:4;
#elif __BYTE_ORDER == __BIG_ENDIAN
	unsigned int version:4;
	unsigned int ihl:4;
#else
# error	"Please fix <bits/endian.h>"
#endif
	u_int8_t tos;
	u_int16_t tot_len;
	u_int16_t id;
	u_int16_t frag_off;
	u_int8_t ttl;
	u_int8_t protocol;
	u_int16_t check;
	union {
		u_int32_t raw;
		struct addr_t addr;
	} u_saddr;
	
	union {
		u_int32_t raw;
		struct addr_t addr;
	} u_daddr;
};


// Main IPQ->QUEUE thread
void *IPQRunner(void *data);


#endif
