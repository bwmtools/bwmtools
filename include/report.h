/*
 * 	report.h - Reporting header file for bwmd
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  25/04/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/


#ifndef _REPORT_H
#define _REPORT_H

#include "common.h"


#define SECS_PER_HOUR 3600
#define SECS_PER_DAY 86400
#define SECS_PER_WEEK 604800

#define FILE_VER_LEN 20

#define FILE_VER_CURRENT FILE_VER_0_2
#define FILE_VER_0_2 "BWM_VER-0.2"

// Report formats we can generate
#define REPORT_FORMAT_NATIVE 0
#define REPORT_FORMAT_RRD 1


// File format stuff...
struct aLogFileHeader_t
{
	char fileVer[FILE_VER_LEN];  // File version
	char ref[MAX_REFLEN];  // Client reference number/string
	long int start;  // Start of file in unix timestamp form
	long int end;  // End of file in unix timestamp form
	long int recs;  // Number of records in the file excluding the header
	int counterTimeout;  // Stepping of counter, ie. how many seconds between writes
};

// File record stuff
struct aLogFileEntry_t
{
	long int unixStampStart;
	long int unixStampEnd;
	int entryType;
	long int bytesSeen;
	long int packetsSeen;
	long int packetsBursted;
	long int packetsDropped;
};

// Function passed structure for logging
struct reportData_t
{
	char *ref;
	int counterTimeout;
	struct aLogFileEntry_t entry;
	char *reportFilename;
};



// Main reporting module
void *reportRunner(void *data);

#endif

