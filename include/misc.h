/*
 * 	misc.h - Misc functions header file
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  15/04/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/


#ifndef _MISC_H
#define _MISC_H


// Macro to add a double   -a ....  parameter to a char ** array
#define ADD_DOUBLE_PARAM(paramList,numParams,param,format,...) \
		{ \
			int i; \
			\
			numParams += 2; \
			paramList = (char **) realloc(paramList, numParams * sizeof(char*)); \
			for (i = numParams - 2; i < numParams; i++) \
				paramList[i] = malloc0(BUFFER_SIZE); \
			strcpy(paramList[numParams - 2], param); \
			snprintf(paramList[numParams - 1], BUFFER_SIZE, format, __VA_ARGS__); \
		}

// Macro to add a single   -a   parameter to a char ** array
#define ADD_SINGLE_PARAM(paramList,numParams,format,...) \
		{ \
			int i; \
			\
			numParams += 1; \
			paramList = (char **) realloc(paramList, numParams * sizeof(char*)); \
			for (i = numParams - 1; i < numParams; i++) \
				paramList[i] = malloc0(BUFFER_SIZE); \
			snprintf(paramList[numParams - 1], BUFFER_SIZE, format, __VA_ARGS__); \
		}


// Convert everything between start & end into an integer
int aptrtoi(char *start, char *end);
	
// Convert datetime format into unix time format
int parseDateTime(char *dateTime);
	
// Format a counter which is in long long int into a nice looking string value
char *counterFMT(char *unit, double value);
	
// Function to send all data to a socket and not just chunks
int sendall(int sock, void *buf, int len);


#endif

