/*
 * 	xmlConf.h - Header stuff for our xml config library
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  05/05/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/


#ifndef _XMLCONF_H
#define _XMLCONF_H

#include <glib.h>
#include <libxml/parser.h>


// An address declaration with all its lil params
struct confClassAddress_t
{
	char *name;
	GHashTable *params;
};

// The main class with all its' addresses
struct confClass_t
{
	char *name;  // Mandatory
	GList *addresses;  // Address item (unparsed)
	GList *ruleset;  // TODO - Parsed list of rules
};


// An ACL list item...
struct confACLTable_t
{
	char *name;  // Mandatory
	GHashTable *chains;  // Chains associated with this table, indexed by chain name
//	GList *ruleset; // TODO - Parsed ruleList items, this is an "array" of char* 's
};

// a chain entry
struct confACLChain_t
{
	char *name; // Mandatory
	char *defaultTarget;  // Optional default target specifier
	GList *ruleList;
	GList *ruleset; // TODO - Parsed ruleList items, this is an "array" of char* 's
};

// a chain rule
struct confACLRule_t
{
	GList *classList; //  Pointless to be NULL - List of classes we using (their NAMES!)
	GHashTable *params;  // What will we do with no params??
	char *target;  // Mandatory - Target of those classes
};



// Function to get a chain if it exists or to create it
struct confACLChain_t *lookupChain(GHashTable *chains, char *chainName);

// Function to get a table if it exists or to create it
struct confACLTable_t *lookupTable(GHashTable *tables, char *tableName);

// Parse the GLOBAL section to get modules we must load
GHashTable *getModuleLoadHash(char *filename);

// Parse the TRAFFIC section and create flows + queues
struct flowData_t createFlowData(char *filename);

// Parse the NAT section
void parseNAT(xmlDocPtr doc, xmlNodePtr cur, GHashTable *fwHash, GHashTable *classHash);

// Parse the TRAFFIC section
void parseTraffic(xmlDocPtr doc, xmlNodePtr cur, GHashTable *fwHash, GHashTable *classHash);

// Parse the GLOBAL section
GHashTable *parseGlobal(xmlDocPtr doc, xmlNodePtr cur);

// Parse the ACL section
GHashTable *parseACL(xmlDocPtr doc, xmlNodePtr cur, GHashTable *fwHash, GHashTable *classHash);


#endif
