/*
 * 	graph.c - BWM Graph functions
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  25/05/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/


#include "../config.h"

#include <errno.h>
#include <fcntl.h>
#include <glib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <rrd.h>

#include "common.h"
#include "graph.h"
#include "misc.h"
#include "report.h"


// Function to create us a proper flow source
struct graphSource_t *createGraphSource(char *flow)
{
	char *flowName = NULL;
	int flowLen;
	char *displayType = NULL;
	int displayTypeLen;
	char *displayTypePos;
	
	char* counter = NULL;
	int counterLen = 0;
	char* leftBracket;
	char* rightBracket;
	
	char *color = NULL;
	char *colorPos;
	int colorLen;
	int totalLen;
	struct graphSource_t *ret;


	// Calculate length & positions
	totalLen = strlen(flow);
	leftBracket = strchr(flow,'(');
	rightBracket = strchr(flow,')');
	displayTypePos = strchr(flow, ':');
	colorPos = (displayTypePos ? strchr(flow, '#') : NULL);
	
	// Check if we have both our brackets or none
	if ((leftBracket && rightBracket))
	{
			flowLen = (int) leftBracket - (int) flow;
			counterLen = (int) rightBracket - (int) leftBracket;
			if (counterLen < 0)
			{
				counterLen = 0;
				fprintf(stderr,"Flow brackets wrong way around?\n");
				return NULL;
			}
	}
	else
		if (!leftBracket && !rightBracket)
		{
			flowLen = (int) displayTypePos ? ((int) displayTypePos - (int) flow) : totalLen;
		}
		else
		{
			fprintf(stderr,"Failed to parse flow\n");
			return NULL;
		}
	
	// Copy in flow
	flowName = strndup(flow,flowLen);

	// Copy in counter to use
	if (counterLen)
		counter = strndup(leftBracket+1,counterLen-1);

	// Check if we have a displayType position
	if (displayTypePos)
	{
		// Calculate length, and copy in
		displayTypeLen = colorPos ? colorPos - displayTypePos - 1 : totalLen - flowLen;
		displayType = strndup(displayTypePos+1,displayTypeLen);
		// Pull in color if we can
		if (colorPos)
		{
			// Same here...
			colorLen = totalLen - displayTypeLen - flowLen;
			color = strndup(colorPos+1,colorLen);
		}
	}


	// Setup return value
	ret = malloc0(sizeof(struct graphSource_t));
	ret->flowRef = flowName;
	ret->flowDisplayType = displayType;
	ret->flowColor = color;
	
	// Decide which counter to use
	ret->flowCounter = COUNTER_NONE;

	// If we have a counter, decipher it
	if (counterLen)
	{
		if (strncasecmp(counter,"pkt",BUFFER_SIZE) == 0)
			ret->flowCounter = COUNTER_PKT;
		if (strncasecmp(counter,"size_bit",BUFFER_SIZE) == 0)
			ret->flowCounter = COUNTER_SIZE_BIT;
		if (strncasecmp(counter,"size_byte",BUFFER_SIZE) == 0 || strncasecmp(counter,"size",BUFFER_SIZE) == 0)
			ret->flowCounter = COUNTER_SIZE_BYTE;
		if (strncasecmp(counter,"dropped",BUFFER_SIZE) == 0)
			ret->flowCounter = COUNTER_DROPPED;
		if (strncasecmp(counter,"bursted",BUFFER_SIZE) == 0)
			ret->flowCounter = COUNTER_BURSTED;
	}
	else
		ret->flowCounter = COUNTER_SIZE_BYTE;

	// We don't have anything we can use
	if (ret->flowCounter == COUNTER_NONE)
	{
		fprintf(stderr,"Counter type not recognized\n");
		free(ret);
		ret = NULL;
	}

	ret->counterEntries = 0;
	ret->counterTotal = 0;
	ret->first = ret->last = -1;
	
	return ret;
}


// Function to write out our rrd files, ready them for graphing
int writeRRDFile(GList *graphSources, struct graphOptions_t *options)
{
	struct aLogFileHeader_t fileHeader;
	struct aLogFileEntry_t *fileEntry;
	int fd;
	int opRes;
	int i,j;
	int err = 0;
	char **params;
	GList *entryList = NULL;
	int numParams = 5;
	char *filename;
	char *rrd_filename;

	
	// Our foreach loop for sources to create the RRD files...
	void processSource(gpointer data, gpointer user_data)
	{
		struct graphSource_t *aSource = (struct graphSource_t*) data;

		
		// Sort func for Glib
		int sort_newest(gconstpointer a, gconstpointer b)
		{
			struct aLogFileEntry_t *entryA = (struct aLogFileEntry_t*) a;
			struct aLogFileEntry_t *entryB = (struct aLogFileEntry_t*) b;

			// Compare the stuff and return so we can sort
			if (entryA->unixStampStart == entryB->unixStampStart) return(0);
			if (entryA->unixStampStart < entryB->unixStampStart) return(-1);
			if (entryA->unixStampStart > entryB->unixStampStart) return(1);

			// Should never happen
			return(0);
		}

		// Process single entry from the list...	
		void write_rrd(gpointer data, gpointer user_data)
		{
			struct aLogFileEntry_t *myEntry = (struct aLogFileEntry_t*) data;
			long int value = 0;


			// Decide which counter to use
			switch (aSource->flowCounter)
			{
				case COUNTER_PKT:
					value = myEntry->packetsSeen;
					break;
				case COUNTER_SIZE_BIT:
				case COUNTER_SIZE_BYTE:
					value = myEntry->bytesSeen;
					break;
				case COUNTER_DROPPED:
					value = myEntry->packetsDropped;
					break;
				case COUNTER_BURSTED:
					value = myEntry->packetsBursted;
					break;
				case COUNTER_NONE:
					fprintf(stderr,"Failed to determine counter type\n");
					break;
			}
		
			// Increase counters
			aSource->counterEntries++;
			if (value > 0)
				aSource->counterAbsTotal += value;

			// Check if we need a value in bits
			if (aSource->flowCounter == COUNTER_SIZE_BIT)
				value *= 8;
			
			aSource->counterTotal += value;
			
			// Create our rrd_update entry
			snprintf(params[4],BUFFER_SIZE,"%li:%li",myEntry->unixStampStart,value);
			// Reset getopt stuff, and carry on
			optind = 0; opterr = 0;	
			opRes = rrd_update(numParams,params);
			if (opRes < 0)
				fprintf(stderr,"Failed to update RRD %s: %i:%s\n",rrd_filename,opRes,rrd_get_error());
			// Free and remove item from our list
			free(myEntry);
			entryList = g_list_remove(entryList,data);
			j++;
		}


		// Bail out if we fuct out
		if (err != 0)
			return;	

		// Loop with all the sources and build the rrd files
		snprintf(filename,MAX_NAMELEN,"%s/%s.dat",LOG_DIR,aSource->flowRef);
		snprintf(rrd_filename,MAX_NAMELEN,"%s/%s.rrd",LOG_DIR,aSource->flowRef);
		printf("    Processing file %s...",filename);
		// Carry on processing
		fd = open(filename,O_RDONLY);
		if (fd < 0)
		{
			printf(" ERROR!\n");
			fprintf(stderr,"ERROR: Failed opening file %s: %s\n",filename,strerror(errno));
			return;
		}
		// Grab header	
		opRes = read(fd,&fileHeader,sizeof(struct aLogFileHeader_t));
		if (opRes != sizeof(struct aLogFileHeader_t))
		{
			printf(" ERROR!\n");
			fprintf(stderr,"ERROR: Error reading header from file %s: %s\n",filename,strerror(errno));
			close(fd);
			return;
		}
		// Process file records
		for (j = 0; j < fileHeader.recs; j++)
		{
			// If no errors seek...
			if (!err)
			{
				// Seek end of file, is this right? should we just seek to recs * recsize + headersize?
				opRes = lseek(fd,sizeof(struct aLogFileHeader_t) + (j * sizeof(struct aLogFileEntry_t)),SEEK_SET);
				if (opRes < 0)
				{
					printf(" ERROR!\n");
					fprintf(stderr,"ERROR: Error seeking end %s: %s\n",filename,strerror(errno));
					err = 1;
					break;
				}
			}
			// Read entry...
			if (!err)
			{
				fileEntry = (struct aLogFileEntry_t *) malloc0(sizeof(struct aLogFileEntry_t));
				
				opRes = read(fd,fileEntry,sizeof(struct aLogFileEntry_t));
				if (opRes < 0)
				{
					printf(" ERROR!\n");
					fprintf(stderr,"Error reading file entry %s: %s\n",filename,strerror(errno));
					err = 1;
					break;
				}
				// Check if entry is in range
				if (fileEntry->unixStampStart >= options->start && fileEntry->unixStampEnd <= options->end)
				{
					// Append to list
					entryList = g_list_append(entryList,fileEntry);
					// Check if we can update our FIRST timestamp seen
					if (fileEntry->unixStampStart < aSource->first || aSource->first == -1)
						aSource->first = fileEntry->unixStampStart;
					// Check if we can update our LAST timestamp seen
					if (fileEntry->unixStampEnd > aSource->last || aSource->last == -1)
						aSource->last = fileEntry->unixStampEnd;
				}
				else
					free(fileEntry);
			}
		}
		// Close file...
		close(fd);
		printf(" sorting...");
		// Sort our list of data...
		entryList = g_list_sort(entryList,sort_newest);
		printf(" generating RRD:%s...",rrd_filename);
		// Set "static" params
		snprintf(params[1],BUFFER_SIZE,"%s",rrd_filename);
		strcpy(params[2],"--template");
		snprintf(params[3],BUFFER_SIZE,"%s",aSource->flowRef);
		// Loop with data
		j = 0;  // Updated in nested function
		g_list_foreach(entryList,write_rrd,aSource->flowRef);
		printf("%i entries\n",j);
	}

	
	// Allocate some filename memory
	filename = (char *) malloc0(MAX_NAMELEN);
	rrd_filename = (char *) malloc0(MAX_NAMELEN);
	
	// Build the params for rrdtool
	// allocated here so we don't malloc, free, malloc each entry above
	params = (char **) malloc0(numParams * sizeof(char*));
	for (i = 0; i < numParams; i++)
		params[i] = (char *) malloc0(BUFFER_SIZE);

	// Set stuff we don't really use, for arguments sake	
	strcpy(params[0],"update");


	i = 0;
	g_list_foreach(graphSources,processSource,NULL);
	

	// Free memory
	for (i = 0; i < numParams; i++)
		free(params[i]);
	free(params);

	free(filename);
	free(rrd_filename);
	
	return(0);
}


// Function to build ourselves the RRD files
int createRRDFiles(GList *graphSources, struct graphOptions_t *options)
{
	char **params;
	int i;
	int result = 0;
	// Number of params we passing below to rrd_create
	int numParams = 11;
	char *filename;
	

	// Our foreach loop for sources to create the RRD files...
	void addRRDFile(gpointer data, gpointer user_data)
	{
		struct graphSource_t *aSource = (struct graphSource_t*) data;
		char type[16];
		int fd, opRes;
		struct aLogFileHeader_t fileHeader;
		

		// Bail out if we fuct up
		if (result != 0)
			return;		

		// Get the filename
		snprintf(filename,MAX_NAMELEN,"%s/%s.dat",LOG_DIR,aSource->flowRef);
		printf("    Processing %s...",filename);
		// Open
		fd = open(filename,O_RDONLY);
		if (fd < 0)
		{
			printf(" ERROR!\n");
			fprintf(stderr,"ERROR: Failed to open %s: %s\n",filename,strerror(errno));
			return;
		}
		// Grab header	
		opRes = read(fd,&fileHeader,sizeof(struct aLogFileHeader_t));
		if (opRes != sizeof(struct aLogFileHeader_t))
		{
			printf(" ERROR!\n");
			fprintf(stderr,"ERROR: Failed to read from %s: %s\n",filename,strerror(errno));
			close(fd);
			return;
		}
		// And set the counter timeout
		aSource->counterTimeout = fileHeader.counterTimeout;
		close(fd);	

		snprintf(filename,MAX_NAMELEN,"%s/%s.rrd",LOG_DIR,aSource->flowRef);
		printf(" RRD:%s...",filename);
		// Create params
		memset(params[5],'\0',BUFFER_SIZE);
		memset(params[6],'\0',BUFFER_SIZE);
		snprintf(params[5],BUFFER_SIZE,"%s",filename);
		// Check which type of graph we want
		switch (aSource->flowCounter)
		{
			case COUNTER_PKT:
				strcpy(type,"GAUGE");
				break;
			case COUNTER_SIZE_BIT:
			case COUNTER_SIZE_BYTE:
				strcpy(type,"ABSOLUTE");
				break;
			case COUNTER_DROPPED:
				strcpy(type,"GAUGE");
				break;
			case COUNTER_BURSTED:
				strcpy(type,"GAUGE");
				break;
			case COUNTER_NONE:
				fprintf(stderr,"Cannot determine counter type\n");
		}
		snprintf(params[6],BUFFER_SIZE,"DS:%s:%s:%i:0:1000000",aSource->flowRef,type,aSource->counterTimeout*4);
		// Call rrd tool
		optind = 0; opterr = 0;	
		result = rrd_create(numParams,params);
		if (result < 0)
		{
			fprintf(stderr,"Failed to create RRD file %s: %s\n",filename,rrd_get_error());
			return;
		}
		printf(" done\n");
		i++;
	}

	
	// Buld the params for rrdtool
	params = (char **) malloc0(numParams * sizeof(char*));
	for (i = 0; i < numParams; i++)
		params[i] = malloc0(BUFFER_SIZE);

	// Allocate here so we can re-use	
	filename = (char *) malloc0(MAX_NAMELEN);

	
	strcpy(params[0],"create");
	strcpy(params[1],"--step");
	snprintf(params[2],BUFFER_SIZE,"%i",RRD_STEP);
	strcpy(params[3],"--start");
	// Minus 10s so we can grab exact matches to the start
	snprintf(params[4],BUFFER_SIZE,"%i",(int) options->start - 10);
	snprintf(params[7],BUFFER_SIZE,"RRA:MAX:0.5:1:5760"); // 2 days working on 30s step
	snprintf(params[8],BUFFER_SIZE,"RRA:MAX:0.5:120:1680");  // 2 weeks, every 1hr
	snprintf(params[9],BUFFER_SIZE,"RRA:MAX:0.5:720:336");  // 3 months,  every 6 hours
	snprintf(params[10],BUFFER_SIZE,"RRA:MAX:0.5:2880:732");  // 2 years, every day
	// Process list...
	g_list_foreach(graphSources,addRRDFile,NULL);
	

	// Free memory
	for (i = 0; i < numParams; i++)
		free(params[i]);
	free(params);
	
	free(filename);

	return(result);
}


// Write our graph, combined graph items and all
int writeGraphs(GList *graphSources, struct graphOptions_t *options)
{
	char **params;
	int i;
	int result = 0;
	char **prdata;
	int xsize, ysize;
	double ymin, ymax;
	int numParams = 0;
	char *lineColors[] = {"ff0033","3300ff","993399","cc9900",NULL};
	int curColor = 0;
	int hasOptions = options->graph_avg || options->graph_total;


	// Create our sources extra options
	void processSourceOptions(gpointer data, gpointer user_data)
	{
		struct graphSource_t *aSource = (struct graphSource_t*) data;

		
		// Add a title if we have options
		if (hasOptions)
			ADD_SINGLE_PARAM(params,numParams,"COMMENT:%s\\c",aSource->flowRef);
	
		// Averages stuff	
		if (options->graph_avg)
		{
			char *buffer;
			char *unit;


			unit = alloca(1024);
			
			switch (aSource->flowCounter)
			{
				case COUNTER_PKT:
				case COUNTER_DROPPED:
				case COUNTER_BURSTED:
					unit = "pkt/s";
					break;
				case COUNTER_SIZE_BIT:
					unit = "bit/s";
					break;
				case COUNTER_SIZE_BYTE:
					unit = "byte/s";
					break;
				case COUNTER_NONE:
					fprintf(stderr,"Cannot determine counter type\n");
			}
			
			buffer = counterFMT(unit, (float) aSource->counterTotal / (float) (aSource->last - aSource->first));
			ADD_SINGLE_PARAM(params,numParams,"COMMENT:Average: %s",buffer);

			free(buffer);
		}

		// Create total line
		if (options->graph_total)
		{
			char *buffer = counterFMT("b", aSource->counterAbsTotal);
			
			
			ADD_SINGLE_PARAM(params,numParams,"COMMENT:Total: %s",buffer);

			free(buffer);
		}

		// Check that if we had options we space them out nicely
		if (hasOptions)
		{
			ADD_SINGLE_PARAM(params,numParams,"%s","COMMENT:\\n");
			ADD_SINGLE_PARAM(params,numParams,"%s","COMMENT:\\n");
		}
		
	}
	
	// Create our graph command
	void processSource(gpointer data, gpointer user_data)
	{
		struct graphSource_t *aSource = (struct graphSource_t*) data;


		// Create definition	
		ADD_SINGLE_PARAM(params,numParams,"DEF:%s=%s/%s.rrd:%s:MAX",aSource->flowRef,LOG_DIR,aSource->flowRef,aSource->flowRef);

		// Check if we have a type, if not create one
		if (!aSource->flowDisplayType)
			aSource->flowDisplayType = strdup("LINE1");
		
		// Same for the color
		if (!aSource->flowColor)
		{
			aSource->flowColor = strdup(lineColors[curColor]);
			curColor++;
			if (!lineColors[curColor])
				curColor = 0;
		}	
		
		// Write our little param to our param list for rrd	
		ADD_SINGLE_PARAM(params,numParams,"%s:%s#%s:%s",aSource->flowDisplayType,aSource->flowRef,aSource->flowColor,aSource->flowRef);
	}


	// Create just a little mem for us to begin with
	params = (char **) malloc0(sizeof(char*));

	// Set location of graph image
	ADD_DOUBLE_PARAM(params,numParams,"graph","%s",options->outputFile);	
	// Set the start and end unix timestamp at the end of the param list... (clever huh?)
	ADD_DOUBLE_PARAM(params,numParams,"--start","%i", (int) options->start);
	ADD_DOUBLE_PARAM(params,numParams,"--end","%i", (int) options->end);

	// Check if we have a title	
	if (options->title)
		ADD_DOUBLE_PARAM(params,numParams,"-t","%s",options->title);

	// Check if we have a vertical title
	if (options->verticalTitle)
		ADD_DOUBLE_PARAM(params,numParams,"-v","%s",options->verticalTitle);

	g_list_foreach(graphSources,processSource,NULL);
	ADD_SINGLE_PARAM(params,numParams,"COMMENT:%s","\\n");
	ADD_SINGLE_PARAM(params,numParams,"COMMENT:%s","\\n");
	// Check if we must display the date
	if (options->graph_date)
	{
		char from_buffer[BUFFER_SIZE], to_buffer[BUFFER_SIZE];
		

		STR_TIME(&options->start,BUFFER_SIZE,from_buffer);
		STR_TIME(&options->end,BUFFER_SIZE,to_buffer);

		ADD_SINGLE_PARAM(params,numParams,"COMMENT:Period %s to %s\\c",from_buffer,to_buffer);
		ADD_SINGLE_PARAM(params,numParams,"COMMENT:%s\\c","- * - * -");
		// Check if we need some spacing
		if (hasOptions)
			ADD_SINGLE_PARAM(params,numParams,"COMMENT:%s","\\n");
	}
	g_list_foreach(graphSources,processSourceOptions,NULL);

	ADD_SINGLE_PARAM(params,numParams,"COMMENT:%s\\c","- * - * -");
	ADD_SINGLE_PARAM(params,numParams,"COMMENT:%s\\c","Graph generated by BWM Tools");
	
	// Call rrd tool
	optind = 0; opterr = 0;
	// Lets make sure we call it correctly
#if (RRDTOOL_VER == 0)
	result = rrd_graph(numParams,params,&prdata,&xsize,&ysize);
#elif (RRDTOOL_VER == 2)
	result = rrd_graph(numParams,params,&prdata,&xsize,&ysize,NULL,&ymin,&ymax);
#else
#warning Your version of rrdtool is not supported
#endif
	if (result < 0)
	{
		printf(" ERROR!\n");
		fprintf(stderr,"ERROR: Failed to create RRD graph %s: %s\n",options->outputFile,rrd_get_error());
		result = -1;
	}
	else
		printf(" done (%dx%d)\n",xsize,ysize);

	// Free memory
	for (i = 0; i < numParams; i++)
		free(params[i]);
	free(params);

	return(result);
}

