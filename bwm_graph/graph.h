/*
 * 	graph.c - BWM Graph functions header file
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  25/05/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/

#ifndef _GRAPH_H
#define _GRAPH_H


#define RRD_STEP 30

#include "common.h"


// Macro to decide the correct date and time
#define STR_TIME(time_p,buffer_size,buffer) \
		{ \
			struct tm *tmp_tm; \
			\
			tmp_tm = localtime(time_p); \
			strftime(buffer,buffer_size,"%d/%m/%Y %T",tmp_tm); \
		}

enum sourceType_e
{
	COUNTER_NONE,
	COUNTER_PKT,
	COUNTER_SIZE_BIT,
	COUNTER_SIZE_BYTE,
	COUNTER_DROPPED,
	COUNTER_BURSTED
};

// Flow source stuff
struct graphSource_t
{
	char *flowRef;
	char *flowDisplayType;
	char *flowColor;
	enum sourceType_e flowCounter;
	
	unsigned long long int counterTotal;
	unsigned long long int counterAbsTotal;
	long int counterEntries;

	int first;
	int last;
	
	int counterTimeout;
};

// Graph options
struct graphOptions_t
{
	char *title;
	char *verticalTitle;
	time_t start;
	time_t end;
	char *outputFile;
	int graph_avg:1;
	int graph_total:1;
	int graph_maxRate:1;
	int graph_date:1;
};


// Function to create us a proper flow source
struct graphSource_t *createGraphSource(char *flow);
// Function to write out our rrd files, ready them for graphing
int writeRRDFile(GList *graphSources, struct graphOptions_t *options);
// Function to build ourselves the RRD files
int createRRDFiles(GList *graphSources, struct graphOptions_t *options);
// Write our graph, combined graph items and all
int writeGraphs(GList *graphSources, struct graphOptions_t *options);
	
#endif
