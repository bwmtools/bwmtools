/*
 * 	bwm_graph.c - BWM Graph
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  26/04/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/

#include "../config.h"

#include <getopt.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"
#include "graph.h"
#include "misc.h"



// Function to print out our usage
void printUsage(char **argv)
{
	printf("Usage: %s <options>\n",argv[0]);
	printf("\n");
	printf("Options:\n");
	printf("    -f,  --flows=<flow1>[(counter)][:<rrd_line_type>[#rrbbgg]][,<flow2>[...]]\n");
	printf("                                    Flow(s) to generate report on\n\n");
	printf("    -s,  --start=\"YYYY/MM/DD HH:MM:SS\"\n");
	printf("                                    Date/Time of report start\n");
	printf("    -e,  --end=\"YYYY/MM/DD HH:MM:SS\"\n");
	printf("                                    Date/Time of report end\n");
	printf("    -h,  --help                     Display this page\n");
	printf("\n");
	printf("Graph Options:\n");
	printf("    --graph-filename=<filename>     Output image filename\n");
	printf("    --graph-avg                     Add averages per flow at bottom of graph\n");
	printf("    --graph-date                    Add date information under graph\n");
	printf("    --graph-title=<graph_title>     Title of graph\n");
	printf("    --graph-total                   Add totals per flow at bottom of graph\n");
//	printf("    --graph-maxRate                 Specify your own maxRate value\n");
	printf("    --graph-vert-title=<graph_title>\n");
   	printf("                                    Vertical title of graph\n");
	printf("\n");
}


// Main processig stuff...
int main(int argc, char **argv)
{
	int c;
	char *flows = NULL;
	// Our options
	struct option long_options[] =
	{
		{"flows",1,0,'f'},
		{"start",1,0,'s'},
		{"end",1,0,'e'},
		{"help",0,0,'h'},
		{"graph-filename",1,0,1},
		{"graph-avg",0,0,2},
		{"graph-date",0,0,3},
		{"graph-title",1,0,4},
		{"graph-total",0,0,5},
//		{"graph-maxRate",1,0,6},
		{"graph-vert-title",1,0,7},
		{0,0,0,0}
	};
	char **flowArray = NULL;
	int result = 0;
	char *aFlow;
	struct graphSource_t *gFlow;
	GList *graphSources = NULL;
	struct graphOptions_t *options;

	
	options = (struct graphOptions_t*) malloc0(sizeof(struct graphOptions_t));
	if (!options)
	{
		fprintf(stderr,"ERROR: Failed to allocate memory options\n");
		return 1;
	}
	
	printf("BWM Graph v%s - Copyright (c) 2003-2006 Linux Based Systems Design\n\n",PACKAGE_VERSION);

	// Print out help
	if (argc == 1)
	{
		printUsage(argv);
		return(1);
	}
	
	// Loop with options
	while (1)
	{
		int option_index = 0;
		
		// Process
		c = getopt_long(argc,argv,"f:s:e:h",long_options,&option_index);
		if (c == -1)
			break;

		// Check...
		switch (c)
		{
			// Normal options
			case 'f':
				// Grab the flows...
				flows = strndup(optarg,BUFFER_SIZE);
				// Split them off
				flowArray = g_strsplit(flows,",",0);
				free(flows);
				break;
			case 's':
				options->start = parseDateTime(optarg);
				if (options->start < 0)
				{
					fprintf(stderr,"ERROR: Error in start time specified\n");
					return(1);
				}
				break;
			case 'e':
				options->end = parseDateTime(optarg);
				if (options->end < 0)
				{
					fprintf(stderr,"ERROR: Error in end time specified\n");
					return(1);
				}
				break;
			case 'h':
				printUsage(argv);
				return 0;

			// Graphing options
			case 1:
				options->outputFile = strndup(optarg,BUFFER_SIZE);
				break;
			case 2:
				options->graph_avg = 1;
				break;
			case 3:
				options->graph_date = 1;
				break;
			case 4:
				options->title = strndup(optarg,BUFFER_SIZE);
				break;
			case 5:
				options->graph_total = 1;
				break;
/*
			case 6:
				options->graph_maxRate = 1;
				break;
*/
			case 7:
				options->verticalTitle = strndup(optarg,BUFFER_SIZE);
				break;

			default:
				printUsage(argv);
				return 1;
		}
		
	}

	// Let us know about any unknown options
	if (optind < argc)
	{
		while (optind < argc)
			fprintf(stderr,"ERROR: Invalid option -- %s\n",argv[optind++]);
		printUsage(argv);
		return 1;
	}

	// Check everything is ok...
	if (options->start <= 0)
	{
		fprintf(stderr,"ERROR: Report start date must be specified\n");
		return 1;
	}
	if (options->end <= 0)
	{
		fprintf(stderr,"ERROR: Report end date must be specified\n");
		return 1;
	}
	if (flowArray == 0)
	{
		fprintf(stderr,"ERROR: No flows specified for reporting\n");
		return 1;
	}

	
	c = 0;
	aFlow = flowArray[c];
	while (aFlow != NULL)
	{
		// Create a proper graph flow
		gFlow = createGraphSource(aFlow);
		if (gFlow)
			// And it to our sources
			graphSources = g_list_append(graphSources,gFlow);
		// advance
		free(aFlow);
		c++;
		aFlow = flowArray[c];
	}
	free(flowArray);

	// Create the files we need...
	printf("Creating RRD files...\n");
	result = createRRDFiles(graphSources,options);
	
	// Write out the RRD files
	if (result == 0)
	{
		printf("Writing data to RRD files...\n");
		result = writeRRDFile(graphSources,options);
	}
	// Graph...
	if (result == 0 && options->outputFile)
	{
		printf("Graphing flows...");
		result = writeGraphs(graphSources,options);
	}
		
	return result;
}




