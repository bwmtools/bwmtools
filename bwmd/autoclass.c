/*
 * 	autoclass.c - Automatic packet priority classification
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  29/08/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/

#include "autoclass.h"
#include "flow.h"



// Auto classify by port
static unsigned char autoClassify_port(struct ip_packet_t *ip_packet, unsigned char prioClassifier)
{
	unsigned char prio = 0;


	// Priority calculation functions, tcp
	unsigned char tcpPortToPrio(u_int16_t port)
	{
		unsigned char ret = 0;
			
			
		// Decide band to pump packet into	
		switch (port)
		{
			// AUTH
			case 113:
				ret = 20;
				break;
			// SSH
			case 22:
			// TELNET
			case 23:
				ret = 25;
				break;
			// HTTP
			case 80:
			// PROXY?
			case 8080:
			case 3128:
			case 3130:
			// HTTPS
			case 443:
				ret = 65;
				break;
			// CVS
			case 2401:
				ret = 70;
				break;
			// POP3
			case 110:
			// IMAP
			case 143:
				ret = 75;
				break;
			// FTP
			case 20:
			case 21:
				ret = 80;
				break;
		};
	
		return ret;
	}

	
	// Priority calculation functions, udp
	unsigned char udpPortToPrio(u_int16_t port)
	{
		unsigned char ret = 0;
		
		
		// Decide band to pump packet into	
		switch (port)
		{
			// DNS
			case 53:
				ret = 10;
				break;
			// NTP
			case 123:
				ret = 15;
				break;
			// RADIUS
			case 1645:
			case 1646:
			case 1812:
			case 1813:
				ret = 30;
				break;
			default:
				// Traceroute	
				if (port >= 33434 && port <= 33465)
					ret = 5;
				break;
		};
	
		return ret;
	}


	// Process a TCP packet
	if (ip_packet->protocol == IPPROTO_TCP)
	{
		struct tcphdr *tcph = (struct tcphdr *) (ip_packet + (ip_packet->ihl * 4));
/*	
		fprintf(stderr,"  tcp -> sport = %i, dport = %i, prec = 0x%x\n", ntohs(tcph->source), 
			ntohs(tcph->dest), IPTOS_PREC(ip_packet->tos));
*/
		if (!(prio = tcpPortToPrio(ntohs(tcph->dest))))
			prio = tcpPortToPrio(ntohs(tcph->source));
	}

	// Process a ICMP packet
	else if (ip_packet->protocol == IPPROTO_ICMP)
	{
//		struct icmphdr *icmph = (struct icmphdr *) (ip_packet + (ip_packet->ihl * 4));

/*
		fprintf(stderr,"something: icmp = %i, type = %i\n", icmph->code, icmph->type);
*/
		prio = 5;
	}

	// Process a UDP packet
	else if (ip_packet->protocol == IPPROTO_UDP)
	{
		struct udphdr *udph = (struct udphdr *) (ip_packet + (ip_packet->ihl * 4));
/*	
		fprintf(stderr,"  udp -> sport = %i, dport = %i\n", ntohs(udph->source), 
			ntohs(udph->dest));
*/
		if (!(prio = udpPortToPrio(ntohs(udph->dest))))
			prio = udpPortToPrio(ntohs(udph->source));
	}

	return prio;
}

	
// Auto classify by ip header TOS value
static unsigned char autoClassify_tos(struct ip_packet_t *ip_packet, unsigned char prioClassifier)
{
	unsigned char prio = 0;
	// Get type of  service
//	unsigned char ecn = AUTOCLASS_TOS_ECN_MASK(ip_packet->tos);
	unsigned char tos = AUTOCLASS_TOS_MASK(ip_packet->tos);
//	unsigned char prec = AUTOCLASS_TOS_PREC_MASK(ip_packet->tos);


	// Decide what we doing...
	if (tos == AUTOCLASS_TOS_LOWDELAY)	
	{
/*
 * 1. LOW DELAY
 *  - PRIO 10
 * 	- high drop probability
 */
		prio = 10;
	}
	else if (tos == AUTOCLASS_TOS_THROUGHPUT)
	{
/*
 * 2. THROUGHPUT
 *  - long queue?
 *  - low drop probability
 */
	}
	else if (tos == AUTOCLASS_TOS_RELIABILITY)
	{
/*
 * 3. RELIABILITY
 *  - low drop probability
 */
	}

	return prio;
}


// Auto classify packet and return priority (1 - best, 100 - worst)
unsigned char autoClassify(struct ip_packet_t *ip_packet, unsigned char prioClassifier)
{
	unsigned char prio;


	// Classify by port
	if (prioClassifier == AUTOCLASS_PORT)
		prio = autoClassify_port(ip_packet,prioClassifier);
	// Classify by TOS
	else if (prioClassifier == AUTOCLASS_TOS)
		prio = autoClassify_tos(ip_packet,prioClassifier);
	// Default - this will basically match if we have AUTOCLASS_NONE
	else
		prio = 50;

	return prio;
}




