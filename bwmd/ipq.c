/*
 * 	ipq.c - IPQ handling for bwmd
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  15/04/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/


#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "autoclass.h"
#include "flow.h"
#include "ipq.h"
#include "libipq.h"



// Destroy the ipq handle
static void destroyIPQHandle(struct ipq_handle *h)
{
	ipq_perror("passer");
	ipq_destroy_handle(h);
}


// Return the ipq handle
static struct ipq_handle *getIPQHandle()
{	
	struct ipq_handle* h;
	int result;

	// Create our ipq socket
	h = ipq_create_handle(0, PF_INET);
	if (!h)
	{
		fprintf(stderr,"Failed to create IPQ handle\n");
		destroyIPQHandle(h);
		return NULL;
	}
	
	// Set mode...
	result = ipq_set_mode(h, IPQ_COPY_PACKET, IPQ_BUFSIZE);
	if (result < 0)
	{
		fprintf(stderr,"Failed to set IPQ mode\n");
		destroyIPQHandle(h);
		return NULL;
	}

	return(h);
}


// Close the ipq handle
static void closeIPQHandle(struct ipq_handle *h)
{
	ipq_destroy_handle(h);
}


// Check if we will exceed our queue
static inline int will_exceed_pkt_queue(struct pktQueue_t *pktQueue, unsigned int pktSize)
{
	return ((pktQueue->curLen >= pktQueue->maxLen && pktQueue->maxLen != 0) ||
		(pktQueue->curSize + pktSize >= pktQueue->maxSize && pktQueue->maxSize != 0));
}


// Queue a packet
static int queuePacket(struct runnerData_t *runnerData, unsigned long int nfmark, struct packet_t *packet)
{
	struct pktQueue_t *foundQueue = NULL;
	struct flow_t *foundFlow = NULL;
	int status;
	int result;
	int drop = 0;
	struct ip_packet_t *ip_packet = (struct ip_packet_t *) packet->payload->payload;


	// Our find functions
	void findQueue(void *data, void *user_data)
	{
		struct pktQueue_t *pktQueue = (struct pktQueue_t*) data;

	
		// Check that we havn't found a queue 
		if (foundQueue) return;
		
		// Sanity check and check if we found it
		if (pktQueue->nfmark && pktQueue->nfmark == nfmark)
			foundQueue = pktQueue;
	}

	void findFlow(void *data, void *user_data)
	{
		struct flow_t *flow = (struct flow_t*) data;

		
		if (foundFlow) return;
		
		// First make sure we have a value... then check if we found it
		if (flow->nfmark && flow->nfmark == nfmark)
			foundFlow = flow;
	}


	// Loop to see what we find... we have to have queues if we going to loop with them
	if (runnerData->pktQueues)
		g_list_foreach(runnerData->pktQueues,findQueue,NULL);
	// Now check if we found a flow??
	if (!foundQueue)
		g_list_foreach(runnerData->flows,findFlow,NULL);

	// Check if we have something
	if (!foundQueue && !foundFlow)
	{
		fprintf(stderr,"Queue or flow not found in processFlow() for %li\n", nfmark);
		return(-1);
	}

	// Now lets see if we don't have a queue... this means AUTO!
	if (!foundQueue)
	{
		unsigned char prio = 0;

		
		prio = autoClassify(ip_packet,foundFlow->prioClassifier);
			
		// If we didn't get anything set band number to 50, 50/50	
		if (prio == 0)
			prio = 50;

		foundQueue = foundFlow->pktQueues[prio];
	}
	
	// Lock flow before we fuck with it
	g_mutex_lock(foundQueue->lock);
	g_mutex_lock(P_FLOW(foundQueue,lock));

	// Check first of all if we fucked over our one of our queue limits
	if (will_exceed_pkt_queue(foundQueue,PKT_SIZE(packet)) || 
			will_exceed_flow_queue(foundQueue->parentFlow,PKT_SIZE(packet)))
		drop = 1;
	// Or checkif we fell over our soft curve, slow start algo
	else
	{
		long int maxQueueSize, curQueueSize, avgQueueSize;


		// FIXME - this is based on the flow's queue size, it should be configurable to the queue's queue size
	   	maxQueueSize = P_FLOW(foundQueue,maxQueueSize);
		curQueueSize = P_FLOW(foundQueue,curQueueSize);
		avgQueueSize = P_FLOW(foundQueue,softQueueSize);
		
		// Check if we have limits to exceed
		if (maxQueueSize && curQueueSize > 0 && avgQueueSize > 0)
		{
			float avgProb = 0, curProb = 0, prob = 0;
			int min_th = 10;
			double drand = drand48();
			

			// nice soft curve flow based on average queue size, starts slow, increases fast will hit 100% probability at 75%	
			avgProb = powf((((float) avgQueueSize / (float) maxQueueSize) * 1.25),3);
			curProb = powf(((float) curQueueSize / (float) maxQueueSize) + 
					powf(((float) min_th / (float) 150),3),2) / 2;
			prob = avgProb + curProb;
			// Check if we should drop packet
			drop = drand < prob;

#if 0		
			// If queue is 10% full, set CE bit if we ECN capable
			if (!drop && (avgQueueSize / curQueueSize) > 0.10)
			{
				unsigned char ecn = AUTOCLASS_TOS_ECN_MASK(ip_packet->tos);


				// We are ECN capable
				if (ecn < 3)
				{
					// Set CE codepoint to communicate congestion
					ip_packet->tos |= AUTOCLASS_TOS_ECN_CE;
					// Tell flowRunner we modified the packet
					packet->changed = 1;
				}
			}
#endif
		}
		
	}
	
	// Check if we must pass the packet
	if (!drop)
	{
		foundQueue->packets = g_list_append(foundQueue->packets,packet);

		foundQueue->curSize += PKT_SIZE(packet);
		P_FLOW(foundQueue,curQueueSize) += PKT_SIZE(packet);
		foundQueue->curLen++;
		P_FLOW(foundQueue,curQueueLen)++;
		result = 1;  // Packet queued
	}
	else
	{
		// Tell kernel to drop packet
		g_mutex_lock(runnerData->IPQLock);
		status = ipq_set_verdict(runnerData->IPQHandle, PKT_ID(packet), NF_DROP, 0, NULL);
		g_mutex_unlock(runnerData->IPQLock);
		if (status < 0)
		{
			fprintf(stderr,"Failed to DROP packet\n");
			result = -1;
		}
		else
		{
			g_mutex_lock(P_FLOW(foundQueue,counterLock));
			P_FLOW(foundQueue,running).pktDropped++;
			g_mutex_unlock(P_FLOW(foundQueue,counterLock));
			result = 2; // Packet dropped
		}
		// Conserve memory
		g_mutex_lock(runnerData->pmem.lock);
		runnerData->pmem.freeList = g_list_append(runnerData->pmem.freeList,packet);
		g_mutex_unlock(runnerData->pmem.lock);
	}

	// Unlock flow
	g_mutex_unlock(P_FLOW(foundQueue,lock));
	g_mutex_unlock(foundQueue->lock);

	// If we have queued, signal a runner
	if (result == 1)
	{
		// Signal that we just added to the queue
		g_mutex_lock(runnerData->bandSignalLock);
		// Check if we havn't already gotten the queue listed
		// FIXME: check if we can't just add, high flow queue
		if (!g_list_find(runnerData->queueChangeList[foundQueue->prio],foundQueue))
			runnerData->queueChangeList[foundQueue->prio] = g_list_append(
					runnerData->queueChangeList[foundQueue->prio],foundQueue);
		runnerData->queueChanged = 1; // This basically signals immediate attention by the flow runner
		g_cond_signal(runnerData->bandSignalCond);
		g_mutex_unlock(runnerData->bandSignalLock);
	}
	
	return(result);
}


// Main IPQ->QUEUE thread
void *IPQRunner(void *data)
{
	unsigned char buf[IPQ_BUFSIZE];
	int status;
	struct runnerData_t *runnerData = (struct runnerData_t*) data;
	ipq_packet_msg_t *m;
	struct packet_t *packet;
	unsigned int pkt_len = 0;
	GList *listItem;

	
	logMessage(LOG_INFO, "IPQ runner started...\n");

	// Get IPQ handle
	runnerData->IPQHandle = getIPQHandle();
	if (!runnerData->IPQHandle)
	{
		logMessage(LOG_ERR, "Failed to get IPQ handle\n");
		return(NULL);
	}

	// And loop
	while (1)
	{
		// Only loop when we have a packet
		status = ipq_read(runnerData->IPQHandle, buf, IPQ_BUFSIZE, 0);

		// Loop if timeout
		if (status == 0)
			continue;
		
		// Report error if we failed to grab the packet
		if (status < 0)
		{
			logMessage(LOG_WARNING, "Failed to get packet from IPQ: %s\n",ipq_errstr());
			continue;
		}

		// We must have a packet... check
		switch (ipq_message_type(buf)) 
		{
			case NLMSG_ERROR:
				logMessage(LOG_WARNING, "Received error message: IPQ = %s, SYSTEM = %s\n",ipq_errstr(),strerror(errno));
				break;

			case IPQM_PACKET: 
				// Get packet details...
				g_mutex_lock(runnerData->IPQLock);
				m = ipq_get_packet(buf);
				g_mutex_unlock(runnerData->IPQLock);
				pkt_len = sizeof(ipq_packet_msg_t) + m->data_len;

				// Get a packet or alloc it
				g_mutex_lock(runnerData->pmem.lock);
				listItem = g_list_first(runnerData->pmem.freeList);
				if (listItem == NULL)
				{
					
					packet = (struct packet_t*) malloc(sizeof(struct packet_t));
					packet->payload = (ipq_packet_msg_t*) malloc(IPQ_BUFSIZE);
				}
				else
				{
					packet = listItem->data;
					runnerData->pmem.freeList = g_list_remove(runnerData->pmem.freeList,packet);
				}
				g_mutex_unlock(runnerData->pmem.lock);
			
				// Copy packet
				memcpy(packet->payload,m,pkt_len);

				// Set packet as not changed
				packet->changed = 0;
				
				// Queue the packet...
				status = queuePacket(runnerData,packet->payload->mark,packet);
				if (status < 0)
					logMessage(LOG_WARNING, "Failed to queue packet\n");
				break;

			default:
				logMessage(LOG_WARNING, "Unknown message type!\n");
				break;
		}
	} 

	closeIPQHandle(runnerData->IPQHandle);

	logMessage(LOG_INFO, "Exiting IPQ runner thread\n");
}


