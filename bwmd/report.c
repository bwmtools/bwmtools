/*
 * 	flowControl.c - BWM Daemon reporting stuff
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  24/04/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/


#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include "flow.h"
#include "misc.h"
#include "report.h"



// Function to write a log entry to the report file
static void *writeNativeEntry(void *data)
{
	struct reportData_t *reportData = (struct reportData_t*) data;
	struct stat statBuf;
	struct aLogFileHeader_t fileHeader;
	int result;
	int opRes;
	int err = 0;
	int fd;


	// Blank file header
	memset(&fileHeader,'\0',sizeof(struct aLogFileHeader_t));
	
	// Get stats for file
	result = stat(reportData->reportFilename,&statBuf);
	if (result == 0)
	{
		// Stats was ok, open it
		fd = open(reportData->reportFilename,O_RDWR);
		if (fd > 0)
		{
			opRes = read(fd,&fileHeader,sizeof(struct aLogFileHeader_t));
			if (opRes != sizeof(struct aLogFileHeader_t))
			{
				logMessage(LOG_ERR, "Error reading header from \"%s\": %s\n",reportData->reportFilename,strerror(errno));
				err = 1;
			}
			// If no errors seek...
			if (!err)
			{
				// Seek end of file, is this right? should we just seek to recs * recsize + headersize?
				opRes = lseek(fd,0,SEEK_END);
				if (opRes < 0)
				{
					logMessage(LOG_ERR, "Error seeking end of file \"%s\": %s\n",reportData->reportFilename,strerror(errno));
					err = 1;
				}
			}
			// Write entry...
			if (!err)
			{
				// Seek end of file, is this right? should we just seek to recs * recsize + headersize?
				opRes = write(fd,&reportData->entry,sizeof(struct aLogFileEntry_t));
				if (opRes != sizeof(struct aLogFileEntry_t))
				{
					logMessage(LOG_ERR, "Error writing record to file \"%s\": %s\n",reportData->reportFilename,strerror(errno));
					err = 1;
				}
				else
				{
					fileHeader.recs++;
					// Check if we were taken before than the header's start
					if (reportData->entry.unixStampStart < fileHeader.start)
						fileHeader.start = reportData->entry.unixStampStart;
					// Check if we were taken later than the header's end
					if (reportData->entry.unixStampStart > fileHeader.end)
						fileHeader.end = reportData->entry.unixStampStart;
				}
			}
			// Go to beginning of file
			if (!err)
			{
				opRes = lseek(fd,0,SEEK_SET);
				if (opRes < 0)
				{
					logMessage(LOG_ERR, "Error seeking beginning of file \"%s\": %s\n",reportData->reportFilename,strerror(errno));
					err = 1;
				}
			}
			// Write header...
			if (!err)
			{
				// Write header at beginning of file
				opRes = write(fd,&fileHeader,sizeof(struct aLogFileHeader_t));
				if (opRes != sizeof(struct aLogFileHeader_t))
				{
					logMessage(LOG_ERR, "Error writing header to file \"%s\": %s\n",reportData->reportFilename,strerror(errno));
					err = 1;
				}
			}
			close(fd);
		}
		else
			logMessage(LOG_ERR,"Failed to open file \"%s\": %s\n",reportData->reportFilename,strerror(errno));
	}
	else
	{
		// Check if file doesn't exist
		if (errno == ENOENT)
		{
			logMessage(LOG_ERR, "Creating report file %s\n",reportData->reportFilename);
			// If not create it
			fd = open(reportData->reportFilename,O_CREAT|O_RDWR,S_IREAD|S_IWRITE);
			if (fd > 0)
			{
				// Build header
				memcpy(&fileHeader.fileVer,FILE_VER_CURRENT,strlen(FILE_VER_CURRENT));
				memcpy(&fileHeader.ref,reportData->ref,strlen(reportData->ref));
				fileHeader.start = reportData->entry.unixStampStart;
				fileHeader.end = reportData->entry.unixStampStart;
				fileHeader.recs = 1;
				fileHeader.counterTimeout = reportData->counterTimeout;
				// Write header
				opRes = write(fd,&fileHeader,sizeof(struct aLogFileHeader_t));
				if (opRes != sizeof(struct aLogFileHeader_t))
				{
					logMessage(LOG_ERR, "Error writing header to \"%s\": %s\n",reportData->reportFilename,strerror(errno));
					err = 1;
				}
				// If no errors write more...
				if (!err)
				{
					opRes = write(fd,&reportData->entry,sizeof(struct aLogFileEntry_t));
					if (opRes != sizeof(struct aLogFileEntry_t))
					{
						logMessage(LOG_ERR, "Error writing record to \"%s\": %s\n",reportData->reportFilename,strerror(errno));
						err = 1;
					}
				}
				close(fd);
			}
			else
				logMessage(LOG_ERR, "Failed to create \"%s\": %s\n",reportData->reportFilename,strerror(errno));
		}
		else
			logMessage(LOG_ERR, "Failed to open file \"%s\": %s\n",reportData->reportFilename,strerror(errno));
	}
	
	return(NULL);
}



// Function to write a log entry to an RRD file
static void *writeRRDEntry(void *data)
{
	struct reportData_t *reportData = (struct reportData_t*) data;
	struct stat statBuf;
	int result;
	int opRes;
	int err = 0;
	int fd;


	// Get stats for file
	result = stat(reportData->reportFilename,&statBuf);
	if (result == 0)
	{
		char **params;
		int numParams = 0;
		int i;


		// Create just a little mem for us to begin with
		params = (char **) malloc0(sizeof(char*));

		// Add update string
		ADD_SINGLE_PARAM(params,numParams,
				"%i@%i:%i:%i:%i",
				reportData->entry.unixStampEnd,
				reportData->entry.bytesSeen,
				reportData->entry.packetsSeen,
				reportData->entry.packetsBursted,
				reportData->entry.packetsDropped
		);
	
		// Try the update	
		if ((i = rrd_update_r(reportData->reportFilename,"bytes:packets:bursted:dropped",numParams,params)) < 0)
			logMessage(LOG_ERR,"Failed to update RRD file %s: %s\n",reportData->reportFilename,rrd_get_error());

		// Free memory
		for (i = 0; i < numParams; i++)
			free(params[i]);
		free(params);
	}
	else
	{
		// Check if file doesn't exist
		if (errno == ENOENT)
			logMessage(LOG_ERR, "Please create \"%s\" first\n",reportData->reportFilename);
		else
			logMessage(LOG_ERR, "Failed to open file \"%s\": %s\n",reportData->reportFilename,strerror(errno));
	}
	
	return(NULL);
}



// Main reporting module
void *reportRunner(void *data)
{
	struct runnerData_t *runnerData = (struct runnerData_t*) data;
	GList *flowItem, *groupItem;
	
	
	logMessage(LOG_DEBUG, "Report runner started...\n");
	// Loop every 1s and reset everything
	while (1)
	{	
		usleep(5000000);


		// Loop with all flows	
		flowItem = g_list_first(runnerData->flows);
		while (flowItem)
		{
			struct flow_t *flow = (struct flow_t*) flowItem->data;
			struct reportData_t reportData;
			int reset = 0;


			g_mutex_lock(flow->counterLock);

			// Take 5s off time remaining
			if (flow->counterTimeout != 0)
			{
				flow->counterRemaining -= 5;
	
				// Check if we gonna pump to disk
				if (flow->counterRemaining < 1)
				{
					// Build our structure to pump into the file
					reportData.ref = flow->flowName;
					reportData.reportFilename = flow->reportFilename;
					reportData.entry.unixStampStart = flow->lastDumpTimestamp;
					flow->lastDumpTimestamp = time(NULL);
					reportData.entry.unixStampEnd = flow->lastDumpTimestamp;
					reportData.counterTimeout = flow->counterTimeout;
					reportData.entry.entryType = flow->counterTimeout;
					reportData.entry.bytesSeen = flow->counter.pktSize;
					reportData.entry.packetsSeen = flow->counter.pktCount;
					reportData.entry.packetsBursted = flow->counter.pktBursted;
					reportData.entry.packetsDropped = flow->counter.pktDropped;

					// Check what format we're working with
					if (flow->reportFormat == REPORT_FORMAT_NATIVE)
						// Log to file
						writeNativeEntry(&reportData);
					else if (flow->reportFormat == REPORT_FORMAT_RRD)
						writeRRDEntry(&reportData);
					else
						logMessage(LOG_ERR, "ERROR: Internal error, report format not understood for \"%s\"\n",flow->flowName);

					// Reset counters
					flow->counterRemaining = flow->counterTimeout;
					reset = 1;
				}
			}
			else
				reset = 1;

			// Check if we need to reset our counters
			if (reset)
			{
				// Reset counters
				flow->counter.pktCount = 0;
				flow->counter.pktSize = 0;
				flow->counter.pktDropped = 0;
				flow->counter.pktBursted = 0;
			}

			g_mutex_unlock(flow->counterLock);

			flowItem = g_list_next(flowItem);
		}



	
		// Loop with all groups
		groupItem = g_list_first(runnerData->groups);
		while (groupItem)
		{
			struct group_t *group = (struct group_t*) groupItem->data;
			struct reportData_t reportData;
			int reset = 0;


			g_mutex_lock(group->counterLock);

			// Take 5s off time remaining
			if (group->counterTimeout != 0)
			{
				group->counterRemaining -= 5;
	
				// Check if we gonna pump to disk
				if (group->counterRemaining < 1)
				{
					// Build our structure and write below
					strncpy(reportData.ref,group->groupName,MAX_REFLEN);
					reportData.entry.unixStampStart = group->lastDumpTimestamp;
					group->lastDumpTimestamp = time(NULL);
					reportData.entry.unixStampEnd = group->lastDumpTimestamp;
					reportData.counterTimeout = group->counterTimeout;
					reportData.entry.entryType = group->counterTimeout;
					reportData.entry.bytesSeen = group->counter.pktSize;
					reportData.entry.packetsSeen = group->counter.pktCount;
					reportData.entry.packetsBursted = group->counter.pktBursted;
					reportData.entry.packetsDropped = group->counter.pktDropped;

					// Check what format we're working with
					if (group->reportFormat == REPORT_FORMAT_NATIVE)
						// Log to file
						writeNativeEntry(&reportData);
					else if (group->reportFormat == REPORT_FORMAT_RRD)
						writeRRDEntry(&reportData);
					else
						logMessage(LOG_ERR, "ERROR: Internal error, report format not understood for \"%s\"\n",group->groupName);

					// Reset counters
					group->counterRemaining = group->counterTimeout;
					reset = 1;
				}
			}
			else
				reset = 1;

			// Check if we need to reset our counters
			if (reset)
			{
				// Reset counters
				group->counter.pktCount = 0;
				group->counter.pktSize = 0;
				group->counter.pktDropped = 0;
				group->counter.pktBursted = 0;
			}

			g_mutex_unlock(group->counterLock);

			groupItem = g_list_next(groupItem);
		}
	}

	return(NULL);	
}

