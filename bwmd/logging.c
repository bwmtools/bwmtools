/*
 * 	logging.c - Logging support for bwmd
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  03/01/2005 - Giang Hu  <freegianghu@gmail.com>
 *      * Initial design
*/

#include <stdarg.h> 
#include <stdio.h>
#include <syslog.h>



char using_syslog = 0;


// Initialize syslog
void initSyslog(const char *ident, int prioMask)
{
	if (!using_syslog)
	{
		openlog(ident, LOG_CONS, LOG_DAEMON);
		setlogmask(LOG_UPTO(prioMask));
		using_syslog = 1;
	}
}


// Log message
void logMessage(int priority, const char *fmt, ...)
{
	va_list	args;


	va_start(args, fmt);

	// Check if we using syslog...	
	if (using_syslog)
		vsyslog(priority, fmt, args);
	// If not and priority is low.. stdout
	else if (priority == 0)
		vfprintf(stdout, fmt, args);
	// If priority is high, stderr
	else
		vfprintf(stderr, fmt, args);

	va_end(args);
}

