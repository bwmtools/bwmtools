/*
 * 	flowControl.c - Backend <-> Frontend control
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  18/04/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/


#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <time.h>
#include "flow.h"
#include "flowControl.h"
#include "misc.h"


#define LISTEN_PORT 9034


// Find a flow by a name
static struct flow_t *findFlowByName(GList *flowList, char *flowName)
{
	GList *flowItem;
	struct flow_t *tmpFlow;


	flowItem = g_list_first(flowList);
	while (flowItem)
	{
		tmpFlow = flowItem->data;
		
		if (strncasecmp(tmpFlow->flowName,flowName,MAX_REFLEN) == 0)
			return tmpFlow;
		
		flowItem = g_list_next(flowItem);
	}

	return NULL;
}

// Find a group by a name
static struct group_t *findGroupByName(GList *groupList, char *groupName)
{
	GList *groupItem;
	struct group_t *tmpGroup;


	groupItem = g_list_first(groupList);
	while (groupItem)
	{
		tmpGroup = groupItem->data;
		
		if (strncasecmp(tmpGroup->groupName,groupName,MAX_REFLEN) == 0)
			return tmpGroup;
		
		groupItem = g_list_next(groupItem);
	}

	return NULL;
}




// Control thread
void *controlRunner(void *data)
{
	struct runnerData_t *runnerData = (struct runnerData_t*) data;
	int i;
	GList *memItem;
	GList *clientItem;
	GList *statusItem;
	GList *flowItem;
	GList *groupItem;
	struct packet_t *tmp;
	fd_set master_fds;
	fd_set temp_fds;
	int serverSock;
        int yes = 1; // setsockopt() SO_REUSEADDR, below
        struct sockaddr_in listenAddr; // Server addr
        int fdmax; // Maximum file descriptor number
	struct timeval now, lastSend, lastFree, lastReset;
	struct ipc_client_t *tmpClient;
	GList *clientList = NULL;
	struct flow_t *tmpFlow;
	struct group_t *tmpGroup;


	// Clear the master and temp sets
        FD_ZERO(&master_fds);
        FD_ZERO(&temp_fds);

        // Get us a socket
        if ((serverSock = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	{
		logMessage(LOG_ERR, "flowControl: Error getting socket: %s\n",strerror(errno));
		return(NULL);
	}

        // Lose the pesky "address already in use" error message
        if (setsockopt(serverSock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) 
	{
		logMessage(LOG_ERR, "flowControl: Error setting socket options: %s\n",strerror(errno));
		return(NULL);
        }

        // Set us up as non blocking
        if (fcntl(serverSock,F_SETFL,O_NONBLOCK))
	{
		logMessage(LOG_ERR, "flowControl: Error setting socket to non-blocking: %s\n",strerror(errno));
		return(NULL);
        }

        // Setup sum stuff we need
        listenAddr.sin_family = AF_INET;
        listenAddr.sin_addr.s_addr = INADDR_ANY;
        listenAddr.sin_port = htons(LISTEN_PORT);
        memset(&(listenAddr.sin_zero), '\0', 8);
	// and bind to it
        if (bind(serverSock, (struct sockaddr *) &listenAddr, sizeof(struct sockaddr)) == -1) 
	{
        	logMessage(LOG_ERR, "flowControl: Error binding to socket: %s\n",strerror(errno));
		return(NULL);
        }

        // And listen on the socket
        if (listen(serverSock, 10) == -1)
	{
		logMessage(LOG_ERR, "flowControl: Failed to listen on socket: %s\n",strerror(errno));
        	return(NULL);
	}

        // Add the server socket  to the master set
        FD_SET(serverSock, &master_fds);

        // Keep track of the biggest file descriptor
        fdmax = serverSock; // so far, it's this one

	logMessage(LOG_DEBUG, "Stat thread started...\n");

	// Init everything to NOW
	gettimeofday(&now,NULL);
	lastSend = lastFree = lastReset = now;
	
	// Loop every 1s and reset everything
	for (;;)
	{
		struct timeval timeout;
		int result;
		struct ipc_packet_t myPacket;


		timeout.tv_sec = 0;
		timeout.tv_usec = 250;

		// Make a copy of our master file descriptors
		temp_fds = master_fds; 
   
	       	// Get the result for the select	
		result = select(fdmax + 1, &temp_fds, NULL, NULL, &timeout);
		    
		if (result == -1) 
		{
			logMessage(LOG_ERR, "flowControl: Failed to call select(): %s\n",strerror(errno));
			return(NULL);
		}

		// Get the time now
		gettimeofday(&now,NULL);

		// We must update clients every half a second
		if (USEC_ELAPSED(now,lastSend,500000))
		{	
			clientItem = g_list_first(clientList);
			while (clientItem)
			{
				tmpClient = clientItem->data;

				// Loop with flowStatusList	
				statusItem = g_list_first(tmpClient->flowStatusList);
				while (statusItem)
				{
					struct status_packet_t statusPacket; 	
					struct pktStat_t tempStat;
	
					tmpFlow = statusItem->data;

					// Construct the status packet and send it away
					strncpy(statusPacket.flowName,tmpFlow->flowName,MAX_REFLEN);
					statusPacket.maxQueueSize = tmpFlow->maxQueueSize;
					statusPacket.maxQueueLen = tmpFlow->maxQueueLen;
					statusPacket.maxRate = tmpFlow->maxRate;
					statusPacket.burstRate = tmpFlow->burstRate;
					statusPacket.running = tmpFlow->running;
					tempStat = getFlowTotalStats(tmpFlow->pktStats,tmpFlow->statsLen);
					statusPacket.avg.pktCount = tempStat.pktCount / tmpFlow->statsLen;
					statusPacket.avg.pktSize = tempStat.pktSize / tmpFlow->statsLen;
					statusPacket.avg.pktBursted = tempStat.pktBursted / tmpFlow->statsLen;
					statusPacket.avg.pktDropped = tempStat.pktDropped / tmpFlow->statsLen;
					// Get the actual queue usage values
					if (tmpFlow->maxQueueLen > 0)
						statusPacket.queueItemUsage = ((float)tmpFlow->curQueueLen / (float)tmpFlow->maxQueueLen) * 100;
					else
						statusPacket.queueItemUsage = 0;
					
					if (tmpFlow->maxQueueSize > 0)
						statusPacket.queueSizeUsage = ((float)tmpFlow->curQueueSize / (float)tmpFlow->maxQueueSize) * 100;
					else
						statusPacket.queueSizeUsage = 0;

					// Setup our status packet
					myPacket.pktType = IPC_PACKET_FLOW_DATA;
					myPacket.u.statusData = statusPacket;
					// And send it away	
					if (sendall(tmpClient->fd, &myPacket, sizeof(struct ipc_packet_t)) == -1) 
						logMessage(LOG_DEBUG, "flowControl: Failed to write to client: %s\n",strerror(errno));
					
					statusItem = g_list_next(statusItem);
				}
				
				// Loop with groupStatusList	
				statusItem = g_list_first(tmpClient->groupStatusList);
				while (statusItem)
				{
					struct status_packet_t statusPacket; 	
					struct pktStat_t tempStat;

					
					tmpGroup = statusItem->data;
			
					// Construct the status packet and send it away
					strncpy(statusPacket.flowName,tmpGroup->groupName,MAX_REFLEN);
					statusPacket.maxQueueSize = -1;
					statusPacket.maxQueueLen = -1;
					statusPacket.maxRate = -1;
					statusPacket.burstRate = -1;
					statusPacket.running = tmpGroup->running;
					tempStat = getFlowTotalStats(tmpGroup->pktStats,tmpGroup->statsLen);
					statusPacket.avg.pktCount = tempStat.pktCount / tmpGroup->statsLen;
					statusPacket.avg.pktSize = tempStat.pktSize / tmpGroup->statsLen;
					statusPacket.avg.pktBursted = tempStat.pktBursted / tmpGroup->statsLen;
					statusPacket.avg.pktDropped = tempStat.pktDropped / tmpGroup->statsLen;
					statusPacket.queueItemUsage = 0;
					statusPacket.queueSizeUsage = 0;

					// Setup our status packet
					myPacket.pktType = IPC_PACKET_FLOW_DATA;
					myPacket.u.statusData = statusPacket;
					// And send it away	
					if (sendall(tmpClient->fd, &myPacket, sizeof(struct ipc_packet_t)) == -1) 
						logMessage(LOG_DEBUG, "flowControl: Failed to write to client: %s\n",strerror(errno));
					
					statusItem = g_list_next(statusItem);
				}
				
				clientItem = g_list_next(clientItem);
			}
			lastSend = now;
		}

		// Check that we didn't timeout
		if (result != 0)
		{
			if (FD_ISSET(serverSock, &temp_fds)) 
			{
				int addrlen, newfd;
				struct sockaddr_in remoteAddr;


				// Handle new connections
				addrlen = sizeof(struct sockaddr);

				// Accept the connection	
				if ((newfd = accept(serverSock, (struct sockaddr *) &remoteAddr, &addrlen)) == -1) 
					logMessage(LOG_DEBUG, "flowControl: Failed to accept connection: %s\n",strerror(errno));
				else
				{
					tmpClient = (struct ipc_client_t*) malloc(sizeof(struct ipc_client_t));

					// Setup our new connection
					tmpClient->fd = newfd;
					tmpClient->flowStatusList = NULL;
					tmpClient->groupStatusList = NULL;
					clientList = g_list_append(clientList,tmpClient);				
							
					// Add fd to master set
					FD_SET(newfd, &master_fds);
					// Keep track of max fd's
					if (newfd > fdmax)
       	                		    fdmax = newfd;
					logMessage(LOG_DEBUG, "flowControl: New connection from %s on socket %d\n", inet_ntoa(remoteAddr.sin_addr), newfd);
				}
			}

			// Loop with all the clients connected
			clientItem = g_list_first(clientList);
			while (clientItem)
			{
				tmpClient = clientItem->data;

				// If the descriptor is set, ie. we have something waiting for us
				if (FD_ISSET(tmpClient->fd, &temp_fds)) 
				{
					int nbytes;

						
					// Handle data from a client
					if ((nbytes = recv(tmpClient->fd, &myPacket, sizeof(struct ipc_packet_t), MSG_NOSIGNAL)) <= 0) 
					{
						// Got error or connection closed by client
						if (nbytes == 0) 
							// Connection closed
							logMessage(LOG_DEBUG, "flowControl: Socket %d hung up\n", tmpClient->fd);
						else 
							logMessage(LOG_DEBUG, "flowControl: Error reading data: %s\n", strerror(errno));
       	        	             		// Close and remove from master set	
						close(tmpClient->fd);
						FD_CLR(tmpClient->fd, &master_fds);
						// Get next item	
						clientItem = g_list_next(clientItem);
						// Remove old client from list, free its update list and memory aswell
						clientList = g_list_remove(clientList,tmpClient);
						g_list_free(tmpClient->flowStatusList);
						free(tmpClient);
					} 
					else
					{
						// Check received packet
						switch (myPacket.pktType)
						{
							case IPC_PACKET_PING:
								// If its a ping packet send them a pong
								myPacket.pktType = IPC_PACKET_PONG;
								time(&myPacket.u.pingData.timestamp);
								sendall(tmpClient->fd, &myPacket, sizeof(struct ipc_packet_t));
								break;
							case IPC_PACKET_PONG:
								logMessage(LOG_DEBUG, "Received a PONG from %i\n",i);
								break;
							case IPC_PACKET_ADD:
								i = 0;
								
								// Add flow to the client connection's flow status list
								tmpFlow = findFlowByName(runnerData->flows,myPacket.u.statusData.flowName);
								if (tmpFlow)
								{
									logMessage(LOG_DEBUG, "Adding flow \"%s\" to update run\n",myPacket.u.statusData.flowName);
									if (!g_list_find(tmpClient->flowStatusList,tmpFlow))
										tmpClient->flowStatusList = g_list_append(tmpClient->flowStatusList,tmpFlow);
									i = 1;
								}
								
								if (!i)
								{
									tmpGroup = findGroupByName(runnerData->groups,myPacket.u.statusData.flowName);
									if (tmpGroup)
									{
										logMessage(LOG_DEBUG, "Adding group \"%s\" to update run\n",myPacket.u.statusData.flowName);
										if (!g_list_find(tmpClient->groupStatusList,tmpGroup))
											tmpClient->groupStatusList = g_list_append(tmpClient->groupStatusList,
													tmpGroup);
										i = 1;
									}
								}

								if (!i)
									logMessage(LOG_DEBUG, "Failed to find flow or group: %s\n",myPacket.u.statusData.flowName);
									
								break;
							case IPC_PACKET_DEL:
								i = 0;
								
								// Delete flow from the client connection's flow status list
								tmpFlow = findFlowByName(runnerData->flows,myPacket.u.statusData.flowName);
								if (tmpFlow)
								{
									logMessage(LOG_DEBUG, "Deleting flow \"%s\" from update run\n",myPacket.u.statusData.flowName);
									tmpClient->flowStatusList = g_list_remove(tmpClient->flowStatusList,tmpFlow);
									i = 1;
								}

								if (!i)
								{
									tmpGroup = findGroupByName(runnerData->groups,myPacket.u.statusData.flowName);
									if (tmpGroup)
									{
										logMessage(LOG_DEBUG, "Deleting group \"%s\" from update run\n",
												myPacket.u.statusData.flowName);
										tmpClient->groupStatusList = g_list_remove(tmpClient->groupStatusList,
												tmpGroup);
										i = 1;
									}
								}

								if (!i)
									logMessage(LOG_DEBUG, "Failed to find flow: %s\n",myPacket.u.statusData.flowName);
								break;
							case IPC_PACKET_LIST_FLOWS:
								// Send flow list to the client
								logMessage(LOG_DEBUG, "Sending flow list\n");
								flowItem = g_list_first(runnerData->flows);
								while (flowItem)
								{
									tmpFlow = flowItem->data;
									
									myPacket.pktType = IPC_PACKET_LIST_ITEM;
									strcpy(myPacket.u.statusData.flowName,tmpFlow->flowName);
									sendall(tmpClient->fd, &myPacket, sizeof(struct ipc_packet_t));
									flowItem = g_list_next(flowItem);
								}
								logMessage(LOG_DEBUG, "Sending group list\n");
								// Send groups aswell, but fake it so it looks like flows
								groupItem = g_list_first(runnerData->groups);
								while (groupItem)
								{
									tmpGroup = groupItem->data;
									
									myPacket.pktType = IPC_PACKET_LIST_ITEM;
									strcpy(myPacket.u.statusData.flowName,tmpGroup->groupName);
									sendall(tmpClient->fd, &myPacket, sizeof(struct ipc_packet_t));
									groupItem = g_list_next(groupItem);
								}
								// And an end packet
								myPacket.pktType = IPC_PACKET_LIST_END;
								sendall(tmpClient->fd, &myPacket, sizeof(struct ipc_packet_t));
								logMessage(LOG_DEBUG, "List ended\n");
								break;	
							case IPC_PACKET_GET_NUM_FLOWS:
								// Reply with number of flows
								myPacket.pktType = IPC_PACKET_NUM_FLOWS;
								myPacket.u.num = g_list_length(runnerData->flows) + g_list_length(runnerData->groups);
								sendall(tmpClient->fd, &myPacket, sizeof(struct ipc_packet_t));
								break;
							default:
								logMessage(LOG_DEBUG, "Received unknown packet type %i\n",myPacket.pktType);
								break;
						}
						clientItem = g_list_next(clientItem);
					}
	                    	}
				else
					clientItem = g_list_next(clientItem);
			}
		}

		// We must free unused mem every 15 seconds
		if (SEC_ELAPSED(now,lastFree,15))
		{
			// Cleanup sum unused memory
			g_mutex_lock(runnerData->pmem.lock);
			i = g_list_length(runnerData->pmem.freeList);
			if (i > 20)
			{
				for (i = 0; i < 5; i++)
				{
					memItem = g_list_last(runnerData->pmem.freeList);
					if (memItem)
					{
						tmp = memItem->data;
						runnerData->pmem.freeList = g_list_remove(runnerData->pmem.freeList,tmp);
						free(tmp->payload);
						free(tmp);
					}
					else
						break;
				}
			}
			g_mutex_unlock(runnerData->pmem.lock);
			lastFree = now;
		}
		
		// Reset running counters
		if (SEC_ELAPSED(now,lastReset,1))
		{
			// Reset flows
			flowItem = g_list_first(runnerData->flows);
			while (flowItem)
			{
				tmpFlow = flowItem->data;

				// Add up for our counter
				g_mutex_lock(tmpFlow->counterLock);
				tmpFlow->counter.pktCount += tmpFlow->running.pktCount;
				tmpFlow->counter.pktSize += tmpFlow->running.pktSize;
				tmpFlow->counter.pktBursted += tmpFlow->running.pktBursted;
				tmpFlow->counter.pktDropped += tmpFlow->running.pktDropped;
				g_mutex_unlock(tmpFlow->counterLock);

				g_mutex_lock(tmpFlow->lock);
				// Statistic stuff...
				tmpFlow->pktStats[tmpFlow->statsPos].pktCount = tmpFlow->running.pktCount;
				tmpFlow->pktStats[tmpFlow->statsPos].pktSize = tmpFlow->running.pktSize;
				tmpFlow->pktStats[tmpFlow->statsPos].pktBursted = tmpFlow->running.pktBursted;
				tmpFlow->pktStats[tmpFlow->statsPos].pktDropped = tmpFlow->running.pktDropped;
				// Log stats, rollover if we have to
				tmpFlow->statsPos++;
				if (tmpFlow->statsPos >= tmpFlow->statsLen)
				tmpFlow->statsPos = 0;

				// Reset our running counter
				tmpFlow->running.pktCount = 0;
				tmpFlow->running.pktSize = 0;
				tmpFlow->running.pktBursted = 0;
				tmpFlow->running.pktDropped = 0;
				g_mutex_unlock(tmpFlow->lock);
		

				flowItem = g_list_next(flowItem);
			}
		
			groupItem = g_list_first(runnerData->groups);
			while (groupItem)
			{
				tmpGroup = groupItem->data;

				// Add up for our counter
				g_mutex_lock(tmpGroup->counterLock);
				tmpGroup->counter.pktCount += tmpGroup->running.pktCount;
				tmpGroup->counter.pktSize += tmpGroup->running.pktSize;
				tmpGroup->counter.pktBursted += tmpGroup->running.pktBursted;
				tmpGroup->counter.pktDropped += tmpGroup->running.pktDropped;
				g_mutex_unlock(tmpGroup->counterLock);

				g_mutex_lock(tmpGroup->lock);
				// Statistic stuff...
				tmpGroup->pktStats[tmpGroup->statsPos].pktCount = tmpGroup->running.pktCount;
				tmpGroup->pktStats[tmpGroup->statsPos].pktSize = tmpGroup->running.pktSize;
				tmpGroup->pktStats[tmpGroup->statsPos].pktBursted = tmpGroup->running.pktBursted;
				tmpGroup->pktStats[tmpGroup->statsPos].pktDropped = tmpGroup->running.pktDropped;
				// Log stats, rollover if we have to
				tmpGroup->statsPos++;
				if (tmpGroup->statsPos >= tmpGroup->statsLen)
				tmpGroup->statsPos = 0;

				// Reset our running counter
				tmpGroup->running.pktCount = 0;
				tmpGroup->running.pktSize = 0;
				tmpGroup->running.pktBursted = 0;
				tmpGroup->running.pktDropped = 0;
				g_mutex_unlock(tmpGroup->lock);
		
				groupItem = g_list_next(groupItem);
			}	

			lastReset = now;
		}
		
	}

	return(NULL);
}

