/*
 * 	flow.c - Flow handling for bwmd
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  15/04/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/


#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "common.h"
#include "flow.h"
#include "ipq.h"


// Function to update all our flow groups
void updateGroups(struct flow_t *flow, int pktCount, int pktSize, int pktDropped, int pktBursted)
{
	GList *groupItem = g_list_first(flow->groups);


	// Loop while we have summin
	while (groupItem)
	{
		struct group_t *tmpGroup = groupItem->data;


		// Lock and load!
		g_mutex_lock(tmpGroup->lock);

		// Do our stuff
		if (pktCount == -1)
			tmpGroup->running.pktCount = 0;
		else
			tmpGroup->running.pktCount += pktCount;

		if (pktSize == -1)
			tmpGroup->running.pktSize = 0;
		else
			tmpGroup->running.pktSize += pktSize;

		if (pktDropped == -1)
			tmpGroup->running.pktDropped = 0;
		else
			tmpGroup->running.pktDropped += pktDropped;

		if (pktBursted == -1)
			tmpGroup->running.pktBursted = 0;
		else
			tmpGroup->running.pktBursted += pktBursted;
			
		g_mutex_unlock(tmpGroup->lock);

		// Next one plz	
		groupItem = g_list_next(groupItem);
	}
}


// Get average stats len, make sure you lock the flow!!!
struct pktStat_t getFlowTotalStats(struct pktStat_t *pktStats, long int pktStatsLen)
{
	struct pktStat_t returnStats;
	long int curStat = 0;

	
	// Reset return values
	returnStats.pktCount = 0;
	returnStats.pktSize = 0;
	returnStats.pktDropped = 0;
	returnStats.pktBursted = 0;
	// Gather stats	
	for (curStat = 0; curStat < pktStatsLen; curStat++)
	{
		returnStats.pktCount += pktStats[curStat].pktCount;
		returnStats.pktSize += pktStats[curStat].pktSize;
		returnStats.pktDropped += pktStats[curStat].pktDropped;
		returnStats.pktBursted += pktStats[curStat].pktBursted;
	}

	// And return
	return returnStats;	
}


/* Set current credit & burst credit */
static inline void calculate_flow_credit(struct flow_t *flow, unsigned int pktSize)
{
	// Check if we limited by rate
	if (flow->maxRate > 0)
	{
		struct timeval curTime;

		// Grab exact time now
		gettimeofday(&curTime,NULL);

		// Calculate credit
		if (curTime.tv_sec - flow->lastCreditCalc.tv_sec > 0)
		{
			flow->curCredit = pktSize;
			// If we must keep track of burst credit, do the same
			if (flow->burstRate > 0)
				flow->curBurstCredit = pktSize;
			gettimeofday(&flow->lastCreditCalc,NULL);
		}
		else
		{
			unsigned int accumCredit;
		

			// Add up accumulated time
			flow->accumMs = curTime.tv_usec - flow->lastCreditCalc.tv_usec;
			// See if we have at lease 1 byte to add
			if ((accumCredit = flow->accumMs * flow->usCredit) > 1)
			{
				// If so add it to max of maxRate
				flow->curCredit += accumCredit;

				// Add on burst credit, if we can
				if (flow->burstRate > 0)
					flow->curBurstCredit += (flow->accumMs * flow->usBurstCredit);

				// Remove the number of microseconds we used
				flow->accumMs -= (accumCredit / flow->usCredit);
				
				// If we exceeded our max's, bring them back down a bit
				if (flow->curCredit > flow->maxRate)
				{
					flow->curCredit = flow->maxRate;
					flow->accumMs = 0;
				}
				if (flow->curBurstCredit > flow->burstRate)
					flow->curBurstCredit = flow->burstRate;

				// Set this as the last time we calculated credit
				gettimeofday(&flow->lastCreditCalc,NULL);
			}
		}
	}
}


// Check if we will exceed our credit
static inline int credit_will_exceed(struct flow_t *flow, unsigned int pktSize)
{
	// Without the typecasts we always get a result of 0 .... *shrug*
	return (flow->maxRate > 0 && (long int) flow->curCredit - (long int) pktSize < 0);
}


// Check if we will exceed our credit
static inline int burst_credit_will_exceed(struct flow_t *flow, unsigned int pktSize)
{
	// If burstRate == 0, it means we can burst to infinity  (limited by parent)
	return (flow->burstRate != 0 && (long int) flow->curBurstCredit - (long int) pktSize < 0);
}



// Function to process a flow queue, returns number of packets accepted
static unsigned int processPktQueue(struct runnerData_t *runnerData, struct pktQueue_t *pktQueue, 
		unsigned int pkts)
{
	int status;
	GList *packets;
	int exceeded = 0;
	int i;
	// Differences in queue when we done
	unsigned int acceptLen = 0, queuedLen = 0;
	unsigned int acceptSize = 0, queuedSize = 0;
	unsigned int processed;
	

	// Lock, hijack packets, unlock
	g_mutex_lock(pktQueue->lock);
	packets = pktQueue->packets;
	pktQueue->packets = NULL;
	g_mutex_unlock(pktQueue->lock);

	// Check that we within our boundaries and will not accept too many packets
	while (!exceeded && pkts > (acceptLen + queuedLen))
	{
		struct packet_t *packet;
		GList *pktQueueItem;
		int ok = 1;
		struct flow_t *flow;
		struct flow_t *childFlow = NULL;


		// Get head of the queue
		pktQueueItem = g_list_first(packets);
		// Last item
		if (!pktQueueItem)
			break;
		// We got summin
		packet = pktQueueItem->data;
	
		
		childFlow = flow = pktQueue->parentFlow;
		// Loop to root
		while (flow && ok)
		{
			int bursted = 0;


			// Set new credits
			calculate_flow_credit(flow,PKT_SIZE(packet));
				
			// Check if we have exceeded stuff we shouldn't	
			if ((i = credit_will_exceed(flow,PKT_SIZE(packet))))
			{

				// If we can burst.... see if exceed anything
				if (flow->burstRate != -1)
				{
					int i;
					
					/* Check if we will exceed our burst credit */
				 	if ((i = burst_credit_will_exceed(flow,PKT_SIZE(packet))))
						ok = 0;
					else
						// Guess we didn't, we bursted
						bursted = 1;
				}
				else
					ok = 0;
			}

			// If we have a parent, if we do... do a few tests
			if (ok && flow->parent)
			{
				// Check if we havn't exceeded our parent queues
				if (will_exceed_flow_queue(flow->parent,PKT_SIZE(packet)))
					ok = 0;

				/*
				 * if (....) - if we here because of bursting, check if we have a 
				 * willing parent to burst to.
				 * 1. We cannot burst over our burst-threshold to the parent
				 * 2. We cannot burst to a parent who is bogged
				 */
				if (ok && bursted) 
				{
					// Calculate parent credits
					calculate_flow_credit(flow->parent,PKT_SIZE(packet));

					// Check if we exceeded
					if (credit_will_exceed(flow->parent,PKT_SIZE(packet)) && 
							burst_credit_will_exceed(flow->parent,PKT_SIZE(packet)))
						ok = 0;
					else
					{
						// Parent threshold stuff, if we havn't exceeded the threshold we can burst
						// First check if we can use the parent_th against the burst rate
						if (flow->parent->burstRate > 0) 
						{
							if (flow->parent_th > 0 && flow->parent_th < (flow->parent->curThroughput / 
									(double) flow->parent->burstRate) * 100.0)
								ok = 0;
						}
						// If not, against the maxRate
						else if (flow->parent->maxRate > 0)
						{
							if (flow->parent_th > 0 && flow->parent_th < (flow->parent->curThroughput /
									(double) flow->parent->maxRate) * 100.0)
								ok = 0;
						}
					}
				}
			}
			
			/* If packet is still ok to pass through
			 * Update our parent threshold stuff so we can calculate parent_th above
			 */	
			if (ok)
			{
				struct timeval curTime;


				g_mutex_lock(flow->lock);
			
				flow->curCredit -= PKT_SIZE(packet);
				flow->accumThroughput += PKT_SIZE(packet);
				flow->accumPackets++;

				// If we can burst ...
				if (flow->burstRate > 0)
					flow->curBurstCredit -= PKT_SIZE(packet);

				// We bursted - this must be direct and not an average
				if (bursted)
					flow->running.pktBursted++;
				
				// Grab exact time now
				gettimeofday(&curTime,NULL);
				
				// Add up accumulated time
				if (curTime.tv_sec - flow->lastThroughputUpdate.tv_sec > 0)
					flow->curThroughputAge += (curTime.tv_sec - flow->lastThroughputUpdate.tv_sec) * 1000000;
				else
					flow->curThroughputAge += curTime.tv_usec - flow->lastThroughputUpdate.tv_usec;

				// 2 seconds
				if (flow->curThroughputAge >= 250000)
				{
					float delta;	
					
					
					// Get the fraction of time passed since last update, predict below to 1 second
					delta = flow->curThroughputAge / 1000000.0;

					// Calculate throughput
					flow->curThroughput = (flow->curThroughput + (flow->accumThroughput / delta)) / 2;
					flow->running.pktSize = flow->curThroughput;
					flow->accumThroughput = 0;
					
					// Calculate packet rate
					flow->curPacketRate = (flow->curPacketRate + (flow->accumPackets / delta)) / 2;
					flow->running.pktCount = flow->curPacketRate;
					flow->accumPackets = 0;
					
					// Calculate queue size
					flow->softQueueSize = (flow->softQueueSize + flow->curQueueSize) / 2;
	
					flow->curThroughputAge = 0;
				}

				// Set this as the last time we updated our throughput
				gettimeofday(&flow->lastThroughputUpdate,NULL);
		

				g_mutex_unlock(flow->lock);
		
				// Time to update our groups
				updateGroups(flow,1,PKT_SIZE(packet),0,0);
				
				// This flow is inbetween, we accepting traffic through, counters must be updated below
				childFlow = flow;
				flow = flow->parent;
			}
		}
		

		// Can we accept the packet
		if (ok)
		{
			// Re-inject packet for processing, check if it was ok...
			g_mutex_lock(runnerData->IPQLock);
			
			// Check if our packet was changed
#if 0
			if (packet->changed)
			{
				struct ip_packet_t *ip_packet = (struct ip_packet_t *) packet->payload->payload;

				// Send packet back modified
				status = ipq_set_verdict(runnerData->IPQHandle, PKT_ID(packet), NF_ACCEPT, ip_packet->tot_len, (unsigned char *) ip_packet);
			}
			else
#endif
				status = ipq_set_verdict(runnerData->IPQHandle, PKT_ID(packet), NF_ACCEPT, 0, NULL);

			g_mutex_unlock(runnerData->IPQLock);
			if (status < 0)
			{
				logMessage(LOG_ERR, "Failed to ACCEPT packet: %s\n",ipq_errstr);
				break;
			}

			// Add to our flow counters, remove from queue and free
			acceptSize += PKT_SIZE(packet);
			acceptLen++;
			packets = g_list_remove(packets,packet);
			// Save the size
			i = PKT_SIZE(packet);
			// "Free" the packet
			g_mutex_lock(runnerData->pmem.lock);
			runnerData->pmem.freeList = g_list_append(runnerData->pmem.freeList,packet);
			g_mutex_unlock(runnerData->pmem.lock);
		}
		else
		{
			// We got no where
			if (flow == childFlow)
				exceeded = 1;
			else
			{
				struct pktQueue_t *nextPktQueue = flow->pktQueues[pktQueue->prio];


				// We can do this before locking
				queuedLen++;
				queuedSize += PKT_SIZE(packet);
				packets = g_list_remove(packets,packet);
			
				// Lock everything
				g_mutex_lock(nextPktQueue->lock);
				g_mutex_lock(flow->lock);
				// Append to parent
				nextPktQueue->packets = g_list_append(nextPktQueue->packets,packet);
				// Update next queue stats	
				nextPktQueue->curLen++;
				nextPktQueue->curSize += PKT_SIZE(packet);
				// Update next flow stats
				flow->curQueueLen++;
				flow->curQueueSize += PKT_SIZE(packet);
				// Finally unlock	
				g_mutex_unlock(flow->lock);
				g_mutex_unlock(nextPktQueue->lock);

				// Signal that we just added to the queue
				g_mutex_lock(runnerData->bandSignalLock);
				// Check if we havn't already gotten the queue listed
				// FIXME: check if we can't just add, high flow queue
				if (!g_list_find(runnerData->queueChangeList[nextPktQueue->prio],nextPktQueue))
					runnerData->queueChangeList[nextPktQueue->prio] = 
						g_list_append(runnerData->queueChangeList[nextPktQueue->prio],nextPktQueue);
				g_cond_signal(runnerData->bandSignalCond);
				g_mutex_unlock(runnerData->bandSignalLock);
			}

		}
	}

	// Lock our data and change it, insert left over packets aswell
	g_mutex_lock(P_FLOW(pktQueue,lock));
	P_FLOW(pktQueue,curQueueLen) -= (acceptLen + queuedLen);
	P_FLOW(pktQueue,curQueueSize) -= (acceptSize + queuedSize);
	g_mutex_unlock(P_FLOW(pktQueue,lock));

	// Update queue aswell
	g_mutex_lock(pktQueue->lock);
	if (packets)
	{
		// Merge the rest in	
		pktQueue->packets = g_list_concat(packets,pktQueue->packets);

		// Signal that we just added to the queue
		g_mutex_lock(runnerData->bandSignalLock);
		// Check if we havn't already gotten the queue listed
		// FIXME: check if we can't just add, high flow queue
		if (!g_list_find(runnerData->queueChangeList[pktQueue->prio],pktQueue))
			runnerData->queueChangeList[pktQueue->prio] = g_list_append(
					runnerData->queueChangeList[pktQueue->prio],pktQueue);
		g_cond_signal(runnerData->bandSignalCond);
		g_mutex_unlock(runnerData->bandSignalLock);
	}

	pktQueue->curLen -= (acceptLen + queuedLen);
	pktQueue->curSize -= (acceptSize + queuedLen);

	g_mutex_unlock(pktQueue->lock);

	processed = acceptLen + queuedLen;
	
	return processed;
}



// Main IPQ->QUEUE thread
void *flowRunner(void *data)
{
	struct runnerData_t *runnerData = (struct runnerData_t*) data;

	
	logMessage(LOG_DEBUG, "Flow runner started...\n");

	// Loop when we get a signal
	while (1)
	{
		GTimeVal mytime;
		unsigned char i;
		GList *queueChangeList[NUM_PRIO_BANDS];


		g_mutex_lock(runnerData->bandSignalLock);

		// If the queue has changed proceed
		if (runnerData->queueChanged == 0)
		{
			g_get_current_time(&mytime);
			g_time_val_add(&mytime,10000);
			g_cond_timed_wait(runnerData->bandSignalCond,runnerData->bandSignalLock,&mytime);
		}
		
		
		// Hijack the queue change list items
		for (i = 0; i < NUM_PRIO_BANDS; i++)
		{
			queueChangeList[i] = NULL;

			// Zero runner data if it is non-NULL
			if (runnerData->queueChangeList[i])
			{
				// Copy list item over
				queueChangeList[i] = g_list_concat(queueChangeList[i],runnerData->queueChangeList[i]);
				// Blank used list... copy uses directly
				runnerData->queueChangeList[i] = NULL;
			}
		}
		
		runnerData->queueChanged = 0;

		g_mutex_unlock(runnerData->bandSignalLock);

		
		// Process list if it is non-NULL
		for (i = 0; i < NUM_PRIO_BANDS; i++)
		{
			// Loop while there are still items to be processed
			while (queueChangeList[i])
			{
				GList *item = g_list_first(queueChangeList[i]);

				// Loop with all queues that need items to be processed
				while (item)
				{
					struct pktQueue_t *pktQueue;
					unsigned int processed = 0;

					// Just a harmless error check
					if ((pktQueue = (struct pktQueue_t*) item->data))
						processed = processPktQueue(runnerData,pktQueue,1);  // Process 1 packet at a time

					// Next queue
					item = g_list_next(item);
					
					// If nothing was processed remove ourselves from the change list
					if (!processed)
						queueChangeList[i] = g_list_remove(queueChangeList[i],(void *) pktQueue);
				}
			}
		}
	}

	return(NULL);
}

	
