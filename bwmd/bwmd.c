/*
 * 	bwmd.c - BWM Daemon
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  18/04/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/

#include <getopt.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include "../config.h"
#include "common.h"
#include "flow.h"
#include "ipq.h"
#include "report.h"
#include "flowControl.h"
#include "xmlConf.h"

// Initialize syslog - This single function header doesn't call for a headerfile
extern void initSyslog(const char *ident, int prioMask);


// Load a module..
void modprobe(gpointer key, gpointer value, gpointer user_data)
{
	char buffer[4096];
	int *outcome = (int *) user_data;
	int ret;

		
	// Generate the command we need
	if (value)
	{
		snprintf(buffer,4096,"/sbin/modprobe %s %s > /dev/null 2>&1", (char *) key, (char *) value);
		fprintf(stderr,"Loading %s with \"%s\"...", (char *) key, (char *) value);
	}
	else
	{
		snprintf(buffer,4096,"/sbin/modprobe %s > /dev/null 2>&1", (char *) key);
		fprintf(stderr,"Loading %s...", (char *) key);
	}
	// Run it and check
	ret = system(buffer);
	if (WEXITSTATUS(ret) != 0)
	{
		fprintf(stderr,"failed\n");
		*outcome = 0;
	}
	else
		fprintf(stderr,"done\n");
}


// Print out our usage
void printUsage(char **argv)
{
	printf("Usage: %s <options>\n",argv[0]);
	printf("\n");
	printf("Options:\n");
	printf("    -c,  --config=<config_file>     Specify non-default BWM Tools config file\n");
	printf("    -f,  --foreground               Run in foreground and print debugging info to screen\n");
	printf("    -h,  --help                     Display this page\n");
	printf("\n");
}


// Main processig stuff...
int main(int argc, char **argv)
{
	struct runnerData_t *runnerData;
	GThread *IPQRunnerThread;
	GThread *flowRunnerThread;
	GThread *controlRunnerThread;
	GThread *reportRunnerThread;
	int daemonize = 1;
	int c, i;
	char *configFile = CONFIG_FILE;
	struct flowData_t flowData;
	GHashTable *moduleHash; //, *moduleItem;


	// Our options
	struct option long_options[] =
	{
		{"config",1,0,'c'},
		{"foreground",0,0,'f'},
		{"help",0,0,'h'},
		{0,0,0,0}
	};
	
	
	printf("BWM Daemon v%s - Copyright (c) 2003-2006 Linux Based Systems Design\n\n",PACKAGE_VERSION);
	
	// Loop with options
	while (1)
	{
		int option_index = 0;
		
		// Process
		c = getopt_long(argc,argv,"c:fh",long_options,&option_index);
		if (c == -1)
			break;

		// Check...
		switch (c)
		{
			case 'c':
				configFile = strdup(optarg);
				break;
			case 'f':
				daemonize = 0;
				break;
			case 'h':
				printUsage(argv);
				return 0;
			default:
				printUsage(argv);
				return 1;
		}
		
	}

	// Let us know about ahy unknown options
	if (optind < argc)
	{
		while (optind < argc)
			fprintf(stderr,"%s: invalid option -- %s\n",argv[0],argv[optind++]);
		return(1);
	}


	// Allocate some memory
	runnerData = (struct runnerData_t*) malloc0(sizeof(struct runnerData_t));
				
	// Initialize thread system
	g_thread_init(NULL);
	if (!g_thread_supported())
	{
		fprintf(stderr,"Failed to initialize thread system\n");
		return(2);
	}


	// Grab our flows...	
	flowData = createFlowData(configFile);
	if (!flowData.flows)
	{
		fprintf(stderr,"Failed to load flow data from configuration file!\n");
		return(3);
	}


	if (!daemonize)
		printf("BWMD: Loaded %i flows and %i queues\n",g_list_length(flowData.flows),
				g_list_length(flowData.pktQueues));

	
	// Time to load our modules
	moduleHash = getModuleLoadHash(configFile);
	if (!moduleHash)
	{
		fprintf(stderr,"Failed to load module list\n");
		return(4);
	}

	if (!daemonize)
		printf("BWMD: Found %i modules to load\n",g_hash_table_size(moduleHash));


	i = 1;
	g_hash_table_foreach(moduleHash,modprobe,&i);
	if (!i)
	{
		fprintf(stderr,"Failed to modprobe module(s)\n");
		return(5);
	}
	g_hash_table_destroy(moduleHash);
	
	// Setup the queue runners'd data
	runnerData->daemon = daemonize;

	runnerData->pktQueues = flowData.pktQueues;
	runnerData->flows = flowData.flows;
	runnerData->groups = flowData.groups;
	
	runnerData->bandSignalLock = g_mutex_new();
	runnerData->bandSignalCond = g_cond_new();
	runnerData->queueChanged = 0;
	runnerData->waitListLock = g_mutex_new();
	runnerData->queueWait = 0;

	// Initialize the arrays
	for (i = 0; i < NUM_PRIO_BANDS; i++)
	{
		runnerData->queueChangeList[i] = NULL;
		runnerData->queueWaitList[i] = NULL;
	}

	runnerData->pmem.freeList = NULL;
	runnerData->pmem.lock = g_mutex_new();

	runnerData->IPQLock = g_mutex_new();

	// Check if we must become a daemon
	if (daemonize)
	{
		initSyslog("bwmd", LOG_DEBUG);
		daemon(0,0);
	}
	// Create our threads to run
	IPQRunnerThread = g_thread_create(IPQRunner, (void *) runnerData, TRUE, NULL);
	flowRunnerThread = g_thread_create(flowRunner, (void *) runnerData, TRUE, NULL);
	controlRunnerThread = g_thread_create(controlRunner, (void *) runnerData, TRUE, NULL);
	reportRunnerThread = g_thread_create(reportRunner, (void *) runnerData, TRUE, NULL);


	g_thread_join(IPQRunnerThread);
	
	
	return 0;
}


