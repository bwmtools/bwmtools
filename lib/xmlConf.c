/*
 * 	xmlConf.c - XML config file stuff
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  05/05/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/


#include "../config.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include "autoclass.h"
#include "common.h"
#include "flow.h"
#include "report.h"
#include "xmlConf.h"



// Function to split out the contents of an xml content into a linked list
static GList *splitContents(xmlNodePtr node)
{
	char *contents = (char *) xmlNodeGetContent(node);
	char **contentArray;
	int i,j,k;
	char *aLine;
	char *bLine;
	GList *result = NULL;


	// Alloc sum memory
	bLine = (char *) malloc(BUFFER_SIZE);
	 
	// Split off contents per line
	contentArray = g_strsplit(contents,"\n",0);
	
	// Loop with all contents
	i = 0;
	aLine = contentArray[i];
	while (aLine)
	{
		// Build bLine out of valid chars	
		memset(bLine,'\0',BUFFER_SIZE);
		k = 0;
		for (j = 0; j < strlen(aLine); j++)
		{
			// Check for valid chars
			if ((aLine[j] >= 48 && aLine[j] <= 57) || (aLine[j] >= 65 && aLine[j] <= 90) || 
					aLine[j] == 95 || (aLine[j] >= 97 && aLine[j] <= 122) || aLine[j] == 124)
			{
				bLine[k] = aLine[j];
				k++;
			}
		}	
		// If we actually got something add it to the list
		if (strlen(bLine) > 0)
		{
			result = g_list_append(result,bLine);
			bLine = (char *) malloc(BUFFER_SIZE);
		}

		// Carry on...
		i++;
		aLine = contentArray[i];
	}

	// Make sure we free the last one
	free(bLine);
	
	return(result);
}


// Function to get the properties in a hash table of a node
static GHashTable *getProperties(xmlNodePtr node)
{
	xmlAttrPtr attrib;
	xmlChar	*attrName;
	xmlChar *contentNode;
	GHashTable *attribHash;
	
	
	attribHash = g_hash_table_new(g_str_hash,g_str_equal);

	// Get an easy to use pointer to our properties and iterate
	attrib = node->properties;
	while (attrib)
	{
		// Save our property to our hash	
		attrName = (xmlChar *) attrib->name;
		contentNode = ((xmlNodePtr) attrib->children)->content;
		g_hash_table_insert(attribHash,attrName,strdup((char *) contentNode));
		// Advance property
		attrib = attrib->next;
	}

	// Return our hash
	return(attribHash);
}


// Function to build a ruleset out of a number of classes and an extra char* at the end
static GList *createRuleset(GHashTable *classHash, GList *classList, char *chainName, char *target, char *cmd_line1, char *cmd_line2)
{
	char *className;
	GList *result = NULL;
	int found;
	
	
	// Function to process ruleset, and append to result
	void processRuleset(struct confClass_t *aClass)
	{
		// Function to append to return ruleset
		void processRulesetItem(gpointer data, gpointer user_data)
		{
			char *rule = (char *) data;
			char *buffer = (char *) malloc0(BUFFER_SIZE);
			int done = 0;

			
			// If we have all
			if (chainName && rule && target && cmd_line1 && cmd_line2)
			{
				snprintf(buffer,BUFFER_SIZE,"-A %s %s %s -j %s %s",chainName,rule,cmd_line1,target,cmd_line2);
				done = 1;
			}
			if (chainName && rule && target && cmd_line1)
			{
				snprintf(buffer,BUFFER_SIZE,"-A %s %s %s -j %s",chainName,rule,cmd_line1,target);
				done = 1;
			}
			if (!done && chainName && rule && target && cmd_line2)
			{
				snprintf(buffer,BUFFER_SIZE,"-A %s %s -j %s %s",chainName,rule,target,cmd_line2);
				done = 1;
			}
			if (!done && chainName && rule && target)
			{
				snprintf(buffer,BUFFER_SIZE,"-A %s %s -j %s",chainName,rule,target);
				done = 1;
			}
			if (!done && cmd_line1)
			{
				snprintf(buffer,BUFFER_SIZE,"%s",cmd_line1);
				done = 1;
			}
			if (!done && cmd_line2)
			{
				snprintf(buffer,BUFFER_SIZE,"%s",cmd_line2);
				done = 1;
			}
	
			// Free buffer if we don't need it	
			if (done)
				result = g_list_append(result,buffer);
			else
				free(buffer);
		}

		// Loop with ruleset items
		g_list_foreach(aClass->ruleset,processRulesetItem,NULL);
	}

	// Function to loop with all classes and find the one we need
	void processClass(gpointer data, gpointer user_data)
	{
		// Function to find a class by name
		void processClassHash(gpointer p_key, gpointer p_value, gpointer user_data)
		{
			char *key = (char *) p_key;
			struct confClass_t *value = (struct confClass_t *) p_value;

			// Check if we found our class
			if (!found)
				if (!strcmp(key,className))
				{
					processRuleset(value);
					found = 1;
				}
		}
	

		className = (char *) data;
		found = 0;
		// First check we have a classHash, if use didn't specify any, this will be NULL */
		if (classHash)
			// Loop with class hash
			g_hash_table_foreach(classHash,processClassHash,NULL);

		// Check if we found it
		if (!found)
			fprintf(stderr,"ERROR: Class %s invalid, ignoring\n",className);
	}


	// Loop through all our classes
	g_list_foreach(classList,processClass,NULL);

	return(result);
}


// Create a classes ruleset
static GList *createClassRuleset(struct confClass_t *confClass)
{
	GList *addresses = confClass->addresses;
	char *buffer;
	char *tmp;
	GList *result = NULL;
	int include = 1;


	// Function to translate each parameter
	void processParams(gpointer p_key, gpointer p_value, gpointer user_data)
	{
		char *key = (char*) p_key;
		char *value = (char*) p_value;
		int done = 0;
		int this_include = 1;
		
		// Clear temp space
		memset(tmp,'\0',BUFFER_SIZE);
	
		// Processing time...	
		if (!strcmp(key,"cmd-line") && !done)
		{
			snprintf(tmp,BUFFER_SIZE," %s ",value);
			strcat(buffer,tmp);
			done = 1;
		}
		if (!strcmp(key,"dst") && !done)
		{
			snprintf(tmp,BUFFER_SIZE,"--destination %s ",value);
			strcat(buffer,tmp);
			done = 1;
		}
		if (!strcmp(key,"dst-iface") && !done)
		{
			snprintf(tmp,BUFFER_SIZE,"--out-interface %s ",value);
			strcat(buffer,tmp);
			done = 1;
		}
		if (!strcmp(key,"dst-port") && !done)
		{
			snprintf(tmp,BUFFER_SIZE,"--destination-port %s ",value);
			strcat(buffer,tmp);
			done = 1;
		}
		if (!strcmp(key,"name") && !done)  // Ignore this one
		{
			done = 1;
			this_include = 0;
		}
		if (!strcmp(key,"proto") && !done)
		{
			snprintf(tmp,BUFFER_SIZE,"--protocol %s ",value);
			strcat(buffer,tmp);
			done = 1;
		}
		if (!strcmp(key,"src") && !done)
		{
			snprintf(tmp,BUFFER_SIZE,"--source %s ",value);
			strcat(buffer,tmp);
			done = 1;
		}
		if (!strcmp(key,"src-iface") && !done)
		{
			snprintf(tmp,BUFFER_SIZE,"--in-interface %s ",value);
			strcat(buffer,tmp);
			done = 1;
		}
		if (!strcmp(key,"src-port") && !done)
		{
			snprintf(tmp,BUFFER_SIZE,"--source-port %s ",value);
			strcat(buffer,tmp);
			done = 1;
		}
	
		// Check if we must complain	
		if (done && include)
			include = 1;
		else
			if (done && !this_include)
			{
				fprintf(stderr,"ERROR: Option \"%s\" no recognized\n",key);
			}
	}


	// We must loop with the addresses somewhere	
	void processAddress(gpointer data, gpointer user_data)
	{
		struct confClassAddress_t *address = (struct confClassAddress_t*) data;

		include = 1;
		// Call for each address & process if we must be included
		g_hash_table_foreach(address->params,processParams,NULL);
		if (include)
			result = g_list_append(result,strndup(buffer,BUFFER_SIZE));
		// Blank buffer to avoid overrun fuckup
		memset(buffer,'\0',BUFFER_SIZE);
	}
	

	// Allocate us sum memory
	buffer = (char *) malloc0(BUFFER_SIZE);
	tmp = (char *) malloc(BUFFER_SIZE);

	// And loop with all addresses
	g_list_foreach(addresses,processAddress,NULL);

	// Cleanup...
	free(tmp);
	free(buffer);
	
	// Returning the rule list 
	return result;	
}

// Function which creates our "special" nat extra cmd_line options
static char *createNATRuleset(char *target, GHashTable *properties)
{
	char *buffer;
	char *tmp;


	// Function which translates the parameters
	void processParams(gpointer p_key, gpointer p_value, gpointer user_data)
	{
		char *key = (char*) p_key;
		char *value = (char*) p_value;
		int done = 0;
		
		// Clear temp space
		memset(tmp,'\0',BUFFER_SIZE);
	
		// Processing time...	
		if (!strcmp(key,"to-src") && !done)
		{
			// Make sure to-src is only used in SNAT section
			if (!strcmp(target, "SNAT"))
			{
				snprintf(tmp,BUFFER_SIZE,"--to-source %s ",value);
				strncat(buffer,tmp,BUFFER_SIZE);
			}
			else
				fprintf(stderr,"ERROR: Tag parameter \"to-src\" is only valid in the <snat> tag. Ignoring!\n");
			
			done = 1;
		}

		if (!strcmp(key,"to-dst") && !done)
		{
			// Make sure to-dst is only used in DNAT section
			if (!strcmp(target, "DNAT"))
			{
				snprintf(tmp,BUFFER_SIZE,"--to-destination %s ",value);
				strncat(buffer,tmp,BUFFER_SIZE);
			}
			else
				fprintf(stderr,"ERROR: Tag parameter \"to-dst\" is only valid in the <dnat> tag. Ignoring!\n");
	
			done = 1;
		}
		
		if (!strcmp(key,"to-ports") && !done)
		{
			// Make sure to-ports is only used in DNAT section
			if (!strcmp(target, "MASQUERADE"))
			{
				snprintf(tmp,BUFFER_SIZE,"--to-ports %s ",value);
				strncat(buffer,tmp,BUFFER_SIZE);
			}
			else
				fprintf(stderr,"ERROR: Tag parameter \"to-ports\" is only valid in the <masq> tag. Ignoring!\n");
	
			done = 1;
		}
		
		if (!strcmp(key,"name") && !done)  // Ignore this one
			done = 1;
	
		// Check if we must complain	
		if (!done)
		{
			fprintf(stderr,"ERROR: Option \"%s\" not recognized. Ignoring!\n",key);
		}
	}

	// Allocate us sum memory
	buffer = (char *) malloc0(BUFFER_SIZE);
	tmp = (char *) malloc(BUFFER_SIZE);

	// And loop with all properties
	g_hash_table_foreach(properties,processParams,NULL);

	// Free sum mem
	free(tmp);
	
	// Return the string 
	return buffer;	
}


// Function which creates our "special" traffic extra cmd_line options
static char *createTrafficRuleset(GHashTable *properties)
{
	char *buffer;
	char *tmp;


	// Function which translates the parameters
	void processParams(gpointer p_key, gpointer p_value, gpointer user_data)
	{
		char *key = (char*) p_key;
		char *value = (char*) p_value;
		int done = 0;
		
		// Clear temp space
		memset(tmp,'\0',BUFFER_SIZE);
	
		// Processing time...	
		if (!done && !strcmp(key,"nfmark"))
		{
			snprintf(tmp,BUFFER_SIZE,"MARK --set-mark %s ",value);
			strcat(buffer,tmp);
			done = 1;
		}
		if (!done && !strcmp(key,"prio"))
			done = 1;
		if (!done && !strcmp(key,"report-timeout"))
			done = 1;
		if (!done && !strcmp(key,"report-format"))
			done = 1;
		if (!done && !strcmp(key,"report-filename"))
			done = 1;
		if (!done && !strcmp(key,"prio-classifier"))
			done = 1;
		if (!done && !strcmp(key,"burst-rate"))
			done = 1;
		if (!done && !strcmp(key,"max-rate"))
			done = 1;
		if (!done && !strcmp(key,"queue-len"))
			done = 1;
		if (!done && !strcmp(key,"queue-size"))
			done = 1;
		if (!done && !strcmp(key,"rate"))
			done = 1;
		if (!done && !strcmp(key,"stats-len"))
			done = 1;
		if (!done && !strcmp(key,"name"))
			done = 1;
		if (!done && !strcmp(key,"burst-threshold"))
			done = 1;
		if (!done && !strcmp(key,"flow-mode"))
			done = 1;
	
		// Check if we must complain	
		if (!done)
		{
			fprintf(stderr,"ERROR: Flow option \"%s\" not recognized. Ignoring!\n",key);
		}
	}

	// Allocate us sum memory
	buffer = (char *) malloc0(BUFFER_SIZE);
	tmp = (char *) malloc(BUFFER_SIZE);

	// And loop with all properties
	g_hash_table_foreach(properties,processParams,NULL);

	// Free sum mem
	free(tmp);
	
	// Return the string 
	return buffer;	
}


// Function to get a chain if it exists or to create it
struct confACLChain_t *lookupChain(GHashTable *chains, char *chainName)
{
	struct confACLChain_t *tmpChain;
	
	// See if we have a table by this name
	tmpChain = g_hash_table_lookup(chains,chainName);
	if (tmpChain == NULL)
	{	
		// If not... create everything
		tmpChain = (struct confACLChain_t*) malloc(sizeof(struct confACLChain_t));
		tmpChain->name = strdup(chainName);
		tmpChain->defaultTarget = NULL;
		// If not... start creating everything...
		tmpChain->ruleList = NULL;
		tmpChain->ruleset = NULL;
		g_hash_table_insert(chains,chainName,tmpChain);
	}

	// Return chain
	return(tmpChain);
}


// Function to get a table if it exists or to create it
struct confACLTable_t *lookupTable(GHashTable *tables, char *tableName)
{
	struct confACLTable_t *tmpTable;
	
	// See if we have a table by this name
	tmpTable = g_hash_table_lookup(tables,tableName);
	if (tmpTable == NULL)
	{	
		// If not... create everything
		tmpTable = (struct confACLTable_t*) malloc(sizeof(struct confACLTable_t));
		tmpTable->name = strdup(tableName);
		tmpTable->chains = g_hash_table_new(g_str_hash,g_str_equal);
		g_hash_table_insert(tables,tableName,tmpTable);
	}

	// Return table
	return(tmpTable);
}


// Parse the GLOBAL section to get modules we must load
GHashTable *getModuleLoadHash(char *filename)
{
	xmlDocPtr doc;
    	xmlNodePtr cur;
	GHashTable *moduleHash = NULL;
	struct stat fstat;
	
	
	moduleHash = g_hash_table_new_full(g_str_hash,g_str_equal,free,free);

    	/* Don't test version, it displays a nasty error
	LIBXML_TEST_VERSION
	*/
	// COMPAT: Do not genrate nodes for formatting spaces
	xmlKeepBlanksDefault(0);
	
	// Check if config file exists
	if (stat(filename,&fstat) != 0)
	{
		fprintf(stderr,"ERROR: Failed to stat configuration path \"%s\": %s\n",filename,strerror(errno));
		return NULL;
	}
	else
		if (!S_ISREG(fstat.st_mode))
		{
			fprintf(stderr,"ERROR: Specified configuration path \"%s\" is not a regular file\n",filename);
			return NULL;
		}
	
	// Build an XML tree from a the file
	doc = xmlParseFile(filename);
    	if (doc == NULL) 
		return NULL;

	// Check the document is of the right kind
	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) 
	{
		fprintf(stderr,"ERROR: Empty document\n");
		xmlFreeDoc(doc);
		return NULL;
	}
	
	// Check if we have the right root element & block
	if (xmlStrcmp(cur->name, (const xmlChar *) "firewall")) 
	{
		fprintf(stderr,"ERROR: Document of the wrong type, root node != firewall\n");
		xmlFreeDoc(doc);
		return NULL;
	}


	// Walk the tree.
	cur = cur->xmlChildrenNode;
	while (cur) 
	{
    		xmlNodePtr myNode = cur;
	
		
		// Try find sections
		if (!xmlStrcmp(myNode->name, (const xmlChar *) "global"))
		{
			// We don't care what the top level element name is...
			myNode = myNode->xmlChildrenNode;
			while (myNode)
			{
				int validTag = 0;


				// Check if we found a class
				if (!xmlStrcmp(myNode->name, (const xmlChar *) "class"))
					validTag = 1;

				// Check if we found modules to load
				if (!xmlStrcmp(myNode->name, (const xmlChar *) "modules"))
				{
					xmlNodePtr moduleNode = myNode->xmlChildrenNode;


					while (moduleNode)
					{
						// Check if we are infact doing an address
						if (!xmlStrcmp(moduleNode->name, (const xmlChar *) "load"))
						{
							char *moduleName = (char *) xmlGetProp(moduleNode, (const xmlChar *) "name");
							char *params = (char *) xmlGetProp(moduleNode, (const xmlChar *) "params");
							int ok = 1;
							

							
							// First check if our name is valid
							if (!moduleName)
							{
								fprintf(stderr,"ERROR: Tag <LOAD /> requires a name. Ignoring!\n");
								ok = 0;
							}

							// Then if we a duplicate
							if (ok && g_hash_table_lookup(moduleHash,moduleName))
							{
								fprintf(stderr,"ERROR: Duplicate module %s in MODULES section. Ignoring!\n",moduleName);
								ok = 0;
							}
					
							// Finally if it is still ok...	
							if (ok)
							{
								// Check if we can dup params
								if (params)
									params = strdup(params);

								// And insert	
								g_hash_table_insert(moduleHash,moduleName,params);
							}
						}
						else
							fprintf(stderr,"WARNING: Unknown tag %s in MODULES section. Ignoring!\n",moduleNode->name);

						moduleNode = moduleNode->next;
					}

					validTag = 1;
				}

				// And a plain text tag...
				if (!xmlStrcmp(myNode->name, (const xmlChar *) "text"))
					validTag = 1;

				// Check if we knew what the tag was
				if (!validTag)
					fprintf(stderr,"WARNING: Unknown tag %s in GLOBAL section. Ignoring!\n",myNode->name);
		
				// Advance "class"
				myNode = myNode->next;
			}
		}

				
	   	// Next plz!! 
		cur = cur->next;
	}


	// Clean up everything else before quitting.
	xmlCleanupParser();

	return(moduleHash);
}


// Create a flow
static struct flow_t* createFlow(
		char *flowName,
		struct flow_t *parentFlow,
		long int statsLen, 
		long int maxQueueSize, 
		long int maxQueueLen, 
		long int maxRate,
		long int burstRate,
		unsigned long int nfmark,
		unsigned char prioClassifier,
		float parent_th,
		int reportFormat,
		int reportTimeout,
		char *reportFilename)
{
	int i;
	struct flow_t *flow = (struct flow_t*) malloc(sizeof(struct flow_t));


	// Check if its ok...
	if (!flow)
	{
		// FIXME - log error
		fprintf(stderr,"Failed to allocate memory for flow\n");
		return NULL;
	}

	// Lock entire flow
	flow->lock = g_mutex_new();
	
	strncpy(flow->flowName,flowName,MAX_REFLEN); // copy string
	
	flow->statsLen = statsLen;
	flow->statsPos = 0;

	flow->parent = parentFlow;

	flow->maxQueueSize = maxQueueSize;
	flow->maxQueueLen = maxQueueLen;
	flow->maxRate = maxRate;
	flow->burstRate = burstRate;
	flow->nfmark = nfmark;

	// Set the priority classifier
	flow->prioClassifier = prioClassifier;

	flow->parent_th = parent_th;
	
	flow->counterTimeout = reportTimeout;
	flow->counterRemaining = reportTimeout;

	flow->curQueueSize = 0;
	flow->curQueueLen = 0;

	// Lock for our counters only
	flow->counterLock = g_mutex_new();
	flow->counter.pktCount = 0;
	flow->counter.pktSize = 0;
	flow->counter.pktDropped = 0;
	flow->counter.pktBursted = 0;

	flow->groups = NULL;
	
	flow->lastDumpTimestamp = time(NULL);
	// Check if we can set our credit
	if (flow->maxRate > 0)
	{
		flow->usCredit = flow->maxRate / 1000000.0;  // For micro (millionths) of seconds silly
		flow->curCredit = 0;
	}
	else
	{
		flow->usCredit = 0;
		flow->curCredit = 0;
	}
	
	if (flow->burstRate > 0)
	{
		flow->usBurstCredit = flow->burstRate / 1000000.0;	
		flow->curBurstCredit = 0;
	}
	else
	{
		flow->usBurstCredit = 0;
		flow->curBurstCredit = 0;
	}
	// Setup throughput stuff...
	gettimeofday(&flow->lastThroughputUpdate,NULL);
	flow->curThroughputAge = 0;
	flow->accumThroughput = 0;
	flow->accumPackets = 0;
	flow->curThroughput = 0;
	flow->curPacketRate = 0;
	flow->softQueueSize = 0;
	// Set last time we calculated credit and the rest...
	flow->accumMs = 0;
	gettimeofday(&flow->lastCreditCalc,NULL);
	// Clear our running counters
	flow->running.pktCount = 0;
	flow->running.pktSize = 0;
	flow->running.pktDropped = 0;
	flow->running.pktBursted = 0;
	
	// Reporting stuff
	flow->reportFormat = reportFormat;
	if (!reportFilename)
	{
		char filename[MAX_NAMELEN];
	

		// Blank filename	
		memset(&filename,'\0',MAX_NAMELEN);

		// Generate our filename
		switch (flow->reportFormat)
		{
				case REPORT_FORMAT_NATIVE:
						snprintf(filename,MAX_NAMELEN,"%s/%s.dat",LOG_DIR,flow->flowName);
						break;

				case REPORT_FORMAT_RRD:
						snprintf(filename,MAX_NAMELEN,"%s/%s.rrd",LOG_DIR,flow->flowName);
						break;
		}

		flow->reportFilename = strdup(filename);
	}
	else
		flow->reportFilename = strdup(reportFilename);	
			
	// Verify everything is ok...
	if (flow->statsLen == 0)
		flow->statsLen = 10;
	
	// Create stuff...
	flow->pktStats = (struct pktStat_t*) malloc0(sizeof(struct pktStat_t) * flow->statsLen);
	if (!flow->pktStats)
	{
		// FIXME - log error
		fprintf(stderr,"Failed to allocate memory for flow->pktStats\n");
		return NULL;
	}

	// Calculate the queue length ... etc
	if (flow->maxQueueLen == -1)
	{
		if (flow->maxRate != 0)
			// max rate / 2 (half a second) / 750 (half of MTU)
			flow->maxQueueLen = ((flow->burstRate + flow->maxRate) / 2 / 750)  * 2;  // Seems an OK value for normal use?
		else
			flow->maxQueueLen = 0;
	}
	if (flow->maxQueueSize == -1)
	{
		if (flow->maxRate != 0)
			flow->maxQueueSize = (flow->burstRate + flow->maxRate);  // Normal use, this should be ok?
		else
			flow->maxQueueSize = 0;
	}

	// Blank all queues
	for (i = 0; i < NUM_PRIO_BANDS; i++) 
	{
		struct pktQueue_t *pktQueue = flow->pktQueues[i] = (struct pktQueue_t*) malloc(sizeof(struct pktQueue_t));
		
		pktQueue->lock = g_mutex_new();
		pktQueue->prio = i;
		pktQueue->parentFlow = flow;
		pktQueue->nfmark = 0;
		pktQueue->curSize = 0;
		pktQueue->curLen = 0;
		pktQueue->maxSize = 0;
		pktQueue->maxLen = 0;
		pktQueue->packets = NULL;
	}

//	printf("Flow added...\n");
//	printf("    flowName       - %s\n",flowName);
//	printf("    parent         - %p\n",parentFlow);
//	printf("    nfmark         - %li\n",nfmark);
//	printf("    statsLen       - %li\n",flow->statsLen);
//	printf("    maxQueueSize   - %li\n",flow->maxQueueSize);
//	printf("    maxQueueLen    - %li\n",flow->maxQueueLen);
//	printf("    maxRate        - %li\n",flow->maxRate);
//	printf("    usCredit       - %.6f\n",flow->usCredit);
//	printf("    curCredit      - %u\n",flow->curCredit);
//	printf("    usBurstCredit  - %.6f\n",flow->usBurstCredit);
//	printf("    curBurstCredit - %u\n",flow->curBurstCredit);
//	printf("    burstRate      - %li\n",flow->burstRate);
//	printf("Queue Stuff:\n");
//	printf("    min_th         - %li\n",flow->min_th);
//	printf("    max_th         - %li\n",flow->max_th);
	
	return flow;	
}


// Create group
static struct group_t *createGroup(
		char *groupName, 
		long int statsLen, 
		int reportFormat, 
		int reportTimeout, 
		char *reportFilename,
		GList *flowList)
{
	struct group_t *group = (struct group_t *) malloc(sizeof(struct group_t));

	
	// Lock entire flow
	group->lock = g_mutex_new();
	
	strncpy(group->groupName,groupName,MAX_REFLEN); // copy string
	
	// Stats stuff
	group->statsLen = statsLen;
	group->statsPos = 0;
	
	// Clear our running counters
	group->running.pktCount = 0;
	group->running.pktSize = 0;
	group->running.pktDropped = 0;
	group->running.pktBursted = 0;
	
	group->counterLock = g_mutex_new();
	group->counter.pktCount = 0;
	group->counter.pktSize = 0;
	group->counter.pktDropped = 0;
	group->counter.pktBursted = 0;

	// Set dump time
	group->lastDumpTimestamp = time(NULL);

	// Report stuff
	group->counterTimeout = reportTimeout;
	group->counterRemaining = reportTimeout;
	group->reportFormat = reportFormat;
	if (!reportFilename)
	{
		char filename[MAX_NAMELEN];
	

		// Blank filename	
		memset(&filename,'\0',MAX_NAMELEN);

		// Generate our filename
		switch (group->reportFormat)
		{
				case REPORT_FORMAT_NATIVE:
						snprintf(filename,MAX_NAMELEN,"%s/%s.dat",LOG_DIR,group->groupName);
						break;

				case REPORT_FORMAT_RRD:
						snprintf(filename,MAX_NAMELEN,"%s/%s.rrd",LOG_DIR,group->groupName);
						break;
		}

		group->reportFilename = strdup(filename);
	}
	else
		group->reportFilename = strdup(reportFilename);	

	// Our list of flows
	group->flowList = flowList;
	
	// Verify everything is ok...
	if (group->statsLen == 0)
		group->statsLen = 10;

	group->pktStats = (struct pktStat_t*) malloc0(sizeof(struct pktStat_t) * group->statsLen);
	if (!group->pktStats)
	{
		// FIXME - log error
		fprintf(stderr,"Failed to allocate memory for group->pktStats\n");
		return NULL;
	}
	return group;
}


// Create a packet queue for the flow
static struct pktQueue_t *createPktQueue(
		long int prio,
		unsigned long int nfmark,
		struct flow_t *parentFlow)
{
	struct pktQueue_t *pktQueue = parentFlow->pktQueues[prio];

	
	// Make sure we valid
	if (prio >= NUM_PRIO_BANDS || prio < 0)
	{
		fprintf(stderr,"ERROR: Queue priority must be between 0 and 99\n");
		return NULL;
	}

	pktQueue->nfmark = nfmark;
/*	
	printf("  Queue added...\n");
	printf("      prio     - %li\n",prio);
	printf("      nfmark   - %li\n",nfmark);
*/	
	return pktQueue;	
}


// Parse the TRAFFIC section and create flows + queues
struct flowData_t createFlowData(char *filename)
{
	xmlDocPtr doc;
   	xmlNodePtr cur;
	GList *pktQueues = NULL;
	GList *flows = NULL, *groups = NULL, *groupItem;
	struct flowData_t ret;


	// Parse a tag... from a node of course
	void parseTag(xmlNodePtr node, struct flow_t *parentFlow)
	{
		struct flow_t *newFlow = NULL;


		// Pull children
		node = node->xmlChildrenNode;
		// Loop
		while (node)
		{
			int validTag = 0;
			GHashTable *tagProperties = getProperties(node);


			// We are a flow...
			if (!xmlStrcmp(node->name, (const xmlChar *) "flow"))
			{
				char *flowName = g_hash_table_lookup(tagProperties,"name");
				int ok = 1;


				// Check everything is ok
				if (!flowName)
				{
					fprintf(stderr,"ERROR: Flow missing the \"name\" property!\n");
					ok = 0;
				}

				
				if (ok)
				{
					char *p, *reportFilename;
					long int statsLen, maxQueueSize, maxQueueLen, maxRate, burstRate, reportTimeout;
					unsigned long int nfmark;
					unsigned char prioClassifier, reportFormat;
					float parent_th;

					
					// Grab all properties we need
					p = g_hash_table_lookup(tagProperties,"stats-len");
					statsLen = p ? atol(p) : 10;
				
					p = g_hash_table_lookup(tagProperties,"queue-size");
					maxQueueSize = p ? atol(p) : -1;
					
					p = g_hash_table_lookup(tagProperties,"queue-len");
					maxQueueLen = p ? atol(p) : -1;

					p = g_hash_table_lookup(tagProperties,"max-rate");
					maxRate = p ? atol(p) : 0;

					p = g_hash_table_lookup(tagProperties,"burst-rate");
					burstRate = p ? atol(p) : -1;
					if (burstRate < maxRate && burstRate > 0)
					{
						fprintf(stderr,"ERROR: %s - Tag \"burst-rate\" cannot be less than max-rate. Setting to -1.\n",flowName);
						burstRate = -1;
					}

					p = g_hash_table_lookup(tagProperties,"nfmark");
					nfmark = p ? atoll(p) : 0;
					
					p = g_hash_table_lookup(tagProperties,"burst-threshold");
					parent_th = p ? atof(p) : -1;
					
					if ((p = g_hash_table_lookup(tagProperties,"report-format")) != NULL)
					{
						if (strcasecmp(p,"native") == 0)
							reportFormat = REPORT_FORMAT_NATIVE;
						else if (strcasecmp(p,"rrd") == 0)
							reportFormat = REPORT_FORMAT_RRD;
						else
							fprintf(stderr,"ERROR: %s - Tag value for \"report-format\" is invalid, please read manual. Ignoring!\n",flowName);
					}
					else
						reportFormat = REPORT_FORMAT_NATIVE;

					p = g_hash_table_lookup(tagProperties,"report-timeout");
					reportTimeout = p ? atol(p) : 0;
					if (reportTimeout < 30 && reportTimeout > 0)
					{
						fprintf(stderr,"ERROR: %s - Tag \"report-timeout\" cannot be below 30 (seconds). Setting to 30 seconds.\n",flowName);
						reportTimeout = 30;
					}
					
					reportFilename = g_hash_table_lookup(tagProperties,"report-filename");
					
					// Work out automatic classifier to use
					if ((p = g_hash_table_lookup(tagProperties,"prio-classifier")) != NULL)
					{
						if (strcasecmp(p,"tos") == 0)
							prioClassifier = AUTOCLASS_TOS; 
						else if (strcasecmp(p,"port") == 0)
							prioClassifier = AUTOCLASS_PORT; 
						else if (strcasecmp(p,"none") == 0)
							prioClassifier = AUTOCLASS_NONE; 
						else
							fprintf(stderr,"ERROR: %s - Tag value for \"prio-classifier\" is invalid, please read manual. Ignoring!\n",flowName);
					}
					else
						prioClassifier = AUTOCLASS_NONE;
					
					// Create our flow
					newFlow = createFlow(flowName,parentFlow,statsLen,maxQueueSize,maxQueueLen,maxRate,burstRate,nfmark,
							prioClassifier,parent_th,reportFormat,reportTimeout,reportFilename);
					
					flows = g_list_append(flows,newFlow);
				}
				validTag = 1;
			}
	
			// Process if we a group of flows
			if (!xmlStrcmp(node->name, (const xmlChar *) "group"))
			{
				char *groupName = g_hash_table_lookup(tagProperties,"name");
				int ok = 1;


				// Check we have a flow name
				if (!groupName)
				{
					fprintf(stderr,"ERROR: Group missing the \"name\" property!\n");
					ok = 0;
				}
				
				if (ok)
				{
					char *p, *reportFilename;
					long int statsLen, reportTimeout;
					unsigned char prioClassifier, reportFormat;
					GList *flowList;
					struct group_t *newGroup;
					
					
					// Grab all properties we need
					p = g_hash_table_lookup(tagProperties,"stats-len");
					statsLen = p ? atol(p) : 10;
				
					if ((p = g_hash_table_lookup(tagProperties,"report-format")) != NULL)
					{
						if (strcasecmp(p,"native") == 0)
							reportFormat = REPORT_FORMAT_NATIVE;
						else if (strcasecmp(p,"rrd") == 0)
							reportFormat = REPORT_FORMAT_RRD;
						else
							fprintf(stderr,"ERROR: %s - Tag value for \"report-format\" is invalid, please read manual. Ignoring!\n",groupName);
					}
					else
						reportFormat = REPORT_FORMAT_NATIVE;
					
					p = g_hash_table_lookup(tagProperties,"report-timeout");
					reportTimeout = p ? atol(p) : 0;
					if (reportTimeout < 30 && reportTimeout > 0)
					{
						fprintf(stderr,"ERROR: %s - Tag \"report-timeout\" cannot be below 30 (seconds)\n",groupName);
						reportTimeout = 30;
					}
				
					reportFilename = g_hash_table_lookup(tagProperties,"report-filename");

					flowList = splitContents(node);
					// Create our group
					newGroup = createGroup(groupName,statsLen,reportFormat,reportTimeout,reportFilename,flowList);
					groups = g_list_append(groups,newGroup);
				}
				validTag = 1;
			}
			
			// Process if we a queue for a flow	
			if (!xmlStrcmp(node->name, (const xmlChar *) "queue"))
			{
				char *queuePrio = g_hash_table_lookup(tagProperties,"prio");
				char *queueNfmark = g_hash_table_lookup(tagProperties,"nfmark");
				int ok = 1;


				// Check everything is ok
				if (!queuePrio)
				{
					fprintf(stderr,"ERROR: Queue missing the \"prio\" property!\n");
					ok = 0;
				}
				if (!queueNfmark)
				{
					fprintf(stderr,"ERROR: Queue missing the \"nfmark\" property!\n");
					ok = 0;
				}

				
				if (ok)
				{
					long int prio;
					unsigned long int nfmark;
					struct pktQueue_t *pktQueue;


					// Grab all properties we need
					prio = atol(queuePrio);
					if (prio >= NUM_PRIO_BANDS || prio < 0)
					{
						fprintf(stderr,"ERROR: Queue priority must be between 0 and 99\n");
						ok = 0;
					}

					nfmark = atoll(queueNfmark);
					
		//			fprintf(stderr,"  Flow queue: %li\n", nfmark);

					if (ok)
					{
						// Create our queue
						pktQueue = createPktQueue(prio,nfmark,parentFlow);
						pktQueues = g_list_append(pktQueues,pktQueue);
					}
				}
				validTag = 1;
			}
			
			// Process if we a queue for a flow	
			if (!xmlStrcmp(node->name, (const xmlChar *) "text"))
				validTag = 1;
		
			// Check if we had a valid tag
			if (validTag)	
				// Parse to see if we have children
				parseTag(node,newFlow);
			else
				fprintf(stderr,"WARNING: Unknown tag %s\n",node->name);
			
			// Advance and continue
			node = node->next;
		}
	}


	// Init...
	ret.pktQueues = NULL;
	ret.flows = NULL;
	ret.groups = NULL;
	
	// Compat, set sum stuff
    	LIBXML_TEST_VERSION
	xmlKeepBlanksDefault(0);

	// Build an XML tree from a the file
	doc = xmlParseFile(filename);
    	if (!doc) 
		return(ret);

	// Check the document is of the right kind
	cur = xmlDocGetRootElement(doc);
	if (!cur) 
	{
		fprintf(stderr,"ERROR: Empty document\n");
		xmlFreeDoc(doc);
		return(ret);
	}
	
	// Check if we have the right root element & block
	if (xmlStrcmp(cur->name, (const xmlChar *) "firewall"))
	{
		fprintf(stderr,"ERROR: Document of the wrong type, root node is not \"firewall\"\n");
		xmlFreeDoc(doc);
		return(ret);
	}

	// Walk the tree.
	cur = cur->xmlChildrenNode;
	while (cur) 
	{
		if (!xmlStrcmp(cur->name, (const xmlChar *) "traffic"))
			parseTag(cur,NULL);
	   	// Next plz!! 
		cur = cur->next;
	}

	// Link flow->groups to the actual groups
	groupItem = g_list_first(groups);
	while (groupItem)
	{
		struct group_t *tmpGroup = groupItem->data;
		GList *contentItem = g_list_first(tmpGroup->flowList);

		// Loop with contentList items
		while (contentItem)
		{
			GList *flowItem = g_list_first(flows);
			int found = 0;
			
			// Loop with flow items
			while (flowItem && !found)	
			{
				struct flow_t *tmpFlow = flowItem->data;

				// Compare flow name to the content list inside the group
				if (!strcmp((char *) contentItem->data, tmpFlow->flowName))
				{
					// If it matches, check we arn't listed twice
					if (!g_list_find(tmpFlow->groups,tmpGroup))
					{
						tmpFlow->groups = g_list_append(tmpFlow->groups,tmpGroup);
						found = 1;
					}
					else
						fprintf(stderr,"WARNINGE: Flow %s listed twice in group %s\n",tmpFlow->flowName,tmpGroup->groupName);
				}
				
				flowItem = g_list_next(flowItem);
			}

			if (!found)
				fprintf(stderr,"WARNING: Failed to find flow %s for group %s\n",(char *) contentItem->data,tmpGroup->groupName);
			
			contentItem = g_list_next(contentItem);
		}
		
		groupItem = g_list_next(groupItem);
	}	
	
	// Cleanup stuff
	xmlCleanupParser();

	// Set our return stuff
	ret.pktQueues = pktQueues;
	ret.flows = flows;
	ret.groups = groups;
	
	return(ret);
}

// Parse the NAT section
void parseNAT(xmlDocPtr doc, xmlNodePtr cur, GHashTable *fwHash, GHashTable *classHash)
{
	char *tableName;
	char *chainName;
	char *ruleName;
	char *target;
	char *cmd_line;
	xmlNodePtr chainNode;
	GList *contentList;
	
	GHashTable *chainHash;
	GList *ruleList;
	struct confACLTable_t *tmpTable; 
	struct confACLChain_t *tmpChain; 
	struct confACLRule_t *tmpRule; 
	GHashTable *tmpProp;


	// Lets start our parsing...
	cur = cur->xmlChildrenNode;
	while (cur)
	{
		tableName = NULL;
		chainName = NULL;
		target = NULL;

		// Check if we found a table...
		if (!xmlStrcmp(cur->name, (const xmlChar *) "masq"))
		{
			tableName = "nat";
			chainName = "POSTROUTING";
			target = "MASQUERADE";
		}	
		if (!xmlStrcmp(cur->name, (const xmlChar *) "snat"))
		{
			tableName = "nat";
			chainName = "POSTROUTING";
			target = "SNAT";
		}	
		if (!xmlStrcmp(cur->name, (const xmlChar *) "dnat"))
		{
			tableName = "nat";
			chainName = "PREROUTING";
			target = "DNAT";
		}	

		// Check we have a table name...
		if (tableName)
		{
			// Find...
			tmpTable = lookupTable(fwHash,tableName);
			chainHash = tmpTable->chains;
		
			// Process our table list...
			chainNode = cur->xmlChildrenNode;
			while (chainNode)
			{
				// Find chain...
				tmpChain = lookupChain(chainHash,chainName);
				ruleList = tmpChain->ruleList;
				tmpChain->defaultTarget = "ACCEPT";
	
				// Check we are in our rule...
				if (!xmlStrcmp(chainNode->name, (const xmlChar *) "rule"))
				{
					tmpProp = getProperties(chainNode);
					ruleName = g_hash_table_lookup(tmpProp,"name");
					contentList = splitContents(chainNode);

					tmpRule = (struct confACLRule_t*) malloc(sizeof(struct confACLRule_t));
					tmpRule->target = target;
					tmpRule->classList = contentList;
					tmpRule->params = tmpProp;

					// Check if everything is ok
					if (tmpRule->target == NULL)
					{
						fprintf(stderr,"ERROR: Internal error, tag <%s> has no target!\n",cur->name);
						free(tmpRule);
					}
					else
					{
						// Get our extra params
						cmd_line = createNATRuleset(tmpRule->target,tmpProp);	
						// Build our ruleSet
						tmpChain->ruleset = g_list_concat(tmpChain->ruleset,
								createRuleset(classHash,tmpRule->classList,
								chainName,tmpRule->target,NULL,cmd_line));
						// Voila! we got it!
						ruleList = g_list_append(ruleList,tmpRule);
					}
				}
					
				// Advance table	
				chainNode = chainNode->next;
			}
		}
		else
			fprintf(stderr,"ERROR: Invalid tag used \"<%s>\" in NAT section, ignored\n",cur->name);

		// Advance our NAT node
		cur = cur->next;
	}

}


// Parse the TRAFFIC section
void parseTraffic(xmlDocPtr doc, xmlNodePtr cur, GHashTable *fwHash, GHashTable *classHash)
{
	char *tableName = "mangle";
	char *target = "";
	GHashTable *chainHash;
	struct confACLTable_t *tmpTable; 
	struct confACLChain_t *aChain; 


	// Parse a rule... from a node of course
	void parseRule(xmlNodePtr node, char *parent)
	{
		GHashTable *tmpProp;
		char *nfmark;
		char *flowMode;
		struct confACLChain_t *tmpChain; 
		char *ruleName;
		char *chainName;
		GList *contentList;
		struct confACLRule_t *tmpRule; 
		char *cmd_line;
		GList *ruleList;


		// Pull children
		node = node->xmlChildrenNode;
		// Loop
		while (node)
		{
			int trafficMatchChain = 0;
			int validTag = 0;
			

			// Grab our properties
			tmpProp = getProperties(node);
			nfmark = g_hash_table_lookup(tmpProp,"nfmark");
			flowMode = g_hash_table_lookup(tmpProp,"flow-mode");

					
			// Check for a flow queue
			if (!xmlStrcmp(node->name, (const xmlChar *) "queue"))
			{
				// Process ourselves
				if (nfmark)
				{
					ruleName = g_hash_table_lookup(tmpProp,"name");

					// trafficMatchChain = 1 means we match for FORWARD traffic
					// trafficMatchChain = 2 means we match for INPUT & OUTPUT traffic
					if (!flowMode)
						trafficMatchChain = 1;
					else
					{
						if (!strcmp(flowMode, "local"))
							trafficMatchChain = 2;
						else if (!strcmp(flowMode, "forward"))
							trafficMatchChain = 1;
					}
					// Check if we managed to decipher the flow-mode parameter
					if (trafficMatchChain == 0)
					{
						fprintf(stderr,"ERROR: flow-mode is invalid!\n");
						goto next;
					}
			
					contentList = splitContents(node);

					tmpRule = (struct confACLRule_t*) malloc(sizeof(struct confACLRule_t));
					tmpRule->target = target;
					tmpRule->classList = contentList;
					tmpRule->params = tmpProp;

					// Check if everything is ok
					if (tmpRule->target == NULL)
					{
						fprintf(stderr,"ERROR: All rules MUST have a target!\n");
						free(tmpRule);
					}
					else
					{
						// Build our ruleSet
						cmd_line = createTrafficRuleset(tmpProp);

						switch (trafficMatchChain)
						{
							case 1:
								// Grab chain...
								tmpChain = lookupChain(chainHash,"FORWARD");
								ruleList = tmpChain->ruleList;
								// Create rule 	
								tmpChain->ruleset = g_list_concat(tmpChain->ruleset,
										createRuleset(classHash,tmpRule->classList,
										"FORWARD",tmpRule->target,NULL,cmd_line));
								// Append rule
								ruleList = g_list_append(ruleList,tmpRule);
								break;

								
							case 2:
								// Grab chain...
								tmpChain = lookupChain(chainHash,"INPUT");
								ruleList = tmpChain->ruleList;
								// Create rule 	
								tmpChain->ruleset = g_list_concat(tmpChain->ruleset,
										createRuleset(classHash,tmpRule->classList,
										"INPUT",tmpRule->target,NULL,cmd_line));
								// Append rule
								ruleList = g_list_append(ruleList,tmpRule);

								// Grab chain...
								tmpChain = lookupChain(chainHash,"OUTPUT");
								ruleList = tmpChain->ruleList;
								// Create rule 	
								tmpChain->ruleset = g_list_concat(tmpChain->ruleset,
										createRuleset(classHash,tmpRule->classList,
										"OUTPUT",tmpRule->target,NULL,cmd_line));
								// Append rule
								ruleList = g_list_append(ruleList,tmpRule);
								break;

						}
						
					}
				}
				validTag = 1;
			}

			if (!xmlStrcmp(node->name, (const xmlChar *) "flow"))
			{
				// Process ourselves
				if (nfmark)
				{
					ruleName = g_hash_table_lookup(tmpProp,"name");

					// trafficMatchChain = 1 means we match for FORWARD traffic
					// trafficMatchChain = 2 means we match for INPUT & OUTPUT traffic
					if (!flowMode)
						trafficMatchChain = 1;
					else
					{
						if (!strcmp(flowMode, "local"))
							trafficMatchChain = 2;
						else if (!strcmp(flowMode, "forward"))
							trafficMatchChain = 1;
					}
					// Check if we managed to decipher the flow-mode parameter
					if (trafficMatchChain == 0)
					{
						fprintf(stderr,"ERROR: flow-mode is invalid!\n");
						goto next;
					}
			
					contentList = splitContents(node);

					tmpRule = (struct confACLRule_t*) malloc(sizeof(struct confACLRule_t));
					tmpRule->target = target;
					tmpRule->classList = contentList;
					tmpRule->params = tmpProp;

					// Check if everything is ok
					if (tmpRule->target == NULL)
					{
						fprintf(stderr,"ERROR: All rules MUST have a target!\n");
						free(tmpRule);
					}
					else
					{
						// Build our ruleSet
						cmd_line = createTrafficRuleset(tmpProp);

						switch (trafficMatchChain)
						{
							case 1:
								// Grab chain...
								tmpChain = lookupChain(chainHash,"FORWARD");
								ruleList = tmpChain->ruleList;
								// Create rule
								tmpChain->ruleset = g_list_concat(tmpChain->ruleset,
										createRuleset(classHash,tmpRule->classList,
										"FORWARD",tmpRule->target,NULL,cmd_line));
								// Append
								ruleList = g_list_append(ruleList,tmpRule);
								break;
							case 2:
								// Grab chain...
								tmpChain = lookupChain(chainHash,"INPUT");
								ruleList = tmpChain->ruleList;
								// Create rule
								tmpChain->ruleset = g_list_concat(tmpChain->ruleset,
										createRuleset(classHash,tmpRule->classList,
										"INPUT",tmpRule->target,NULL,cmd_line));
								// Append
								ruleList = g_list_append(ruleList,tmpRule);

								// Grab chain...
								tmpChain = lookupChain(chainHash,"OUTPUT");
								ruleList = tmpChain->ruleList;
								// Create rule
								tmpChain->ruleset = g_list_concat(tmpChain->ruleset,
										createRuleset(classHash,tmpRule->classList,
										"OUTPUT",tmpRule->target,NULL,cmd_line));
								// Append
								ruleList = g_list_append(ruleList,tmpRule);
								break;
						}	
					}
				}
				// Parse child flow... see if its embedded
				parseRule(node,g_hash_table_lookup(tmpProp,"name"));
				validTag = 1;
			}
		
			// Check for the tags we don't really use here...
			if (!xmlStrcmp(node->name, (const xmlChar *) "group"))
				validTag = 1;

			if (!xmlStrcmp(node->name, (const xmlChar *) "text"))
				validTag = 1;

			// Check if the tag we had was valid
			if (!validTag)
				fprintf(stderr,"WARNING: Unknown tag %s\n",node->name);

next:		
			node = node->next;
		}
	}


	// See if we have a table by this name
	tmpTable = lookupTable(fwHash,"filter");
	// Check if we already did the chain or not
	aChain = lookupChain(tmpTable->chains,"bwmd");
	// Add to our ruleset
	aChain->ruleset = g_list_append(aChain->ruleset,"-A bwmd -m mark ! --mark 0 -j QUEUE");


	tmpTable = lookupTable(fwHash,tableName);
	chainHash = tmpTable->chains;
	
	// Parse our rules...
	parseRule(cur,NULL);
}


// Parse the GLOBAL section
GHashTable *parseGlobal(xmlDocPtr doc, xmlNodePtr cur)
{
	char *className;
	xmlNodePtr classNode;
	GHashTable *classHash;
	struct confClass_t *tempClass;
	struct confClassAddress_t *tempClassAddress;
	

	classHash = g_hash_table_new(g_str_hash,g_str_equal);

	// We don't care what the top level element name is...
	cur = cur->xmlChildrenNode;
	while (cur)
	{
		int validTag = 0;


		// Check if we found a class
		if (!xmlStrcmp(cur->name, (const xmlChar *) "class"))
		{
			className = (char *) xmlGetProp(cur, (const xmlChar *) "name");
			// We have found a class!!
			if (className)
			{
				if (g_hash_table_lookup(classHash,className) == NULL)
				{
					// Allocate our class
					tempClass = (struct confClass_t*) malloc(sizeof(struct confClass_t));
					tempClass->name = className;
					tempClass->addresses = NULL;
				
					// Loop with class contents
					classNode = cur->xmlChildrenNode;
					while (classNode)
					{

						// Check if we are infact doing an address
						if (!xmlStrcmp(classNode->name, (const xmlChar *) "address"))
						{
							// Allocate a new address structure
							tempClassAddress = (struct confClassAddress_t*) malloc(sizeof(struct confClassAddress_t));
							tempClassAddress->params = getProperties(classNode);
							// FIXME - get this things name from the hash above
							tempClassAddress->name = NULL;
							// FIXME - we should parse the attribs here??
						
							tempClass->addresses = g_list_append(tempClass->addresses,tempClassAddress);
						}
						// Advance "address"
						classNode = classNode->next;
					}
				
					// Create our ruleset
					tempClass->ruleset = createClassRuleset(tempClass);
						
					// Add our class to the major list...
					g_hash_table_insert(classHash,className,tempClass);
				}
				else
					fprintf(stderr,"ERROR: Duplicate class found: %s\n",className);
			}
			else
				fprintf(stderr,"ERROR: We found a class without a name\n");
			validTag = 1;
		}

		// Check if we found modules to load
		if (!xmlStrcmp(cur->name, (const xmlChar *) "modules"))
			validTag = 1;

		// And a plain text tag...
		if (!xmlStrcmp(cur->name, (const xmlChar *) "text"))
			validTag = 1;

		// Check if we knew what the tag was
		if (!validTag)
			fprintf(stderr,"WARNING: Unknown tag %s in GLOBAL section\n",cur->name);
		
		// Advance "class"
		cur = cur->next;
	}

	return(classHash);
}


// Parse the ACL section
GHashTable *parseACL(xmlDocPtr doc, xmlNodePtr cur, GHashTable *fwHash, GHashTable *classHash)
{
	char *tableName;
	char *chainName;
	char *ruleName;
	xmlNodePtr tableNode;
	xmlNodePtr chainNode;
	GList *contentList;
	GHashTable *chainHash;
	GList *ruleList;
	struct confACLTable_t *tmpTable; 
	struct confACLChain_t *tmpChain; 
	struct confACLRule_t *tmpRule; 
	GHashTable *tmpProp;


	// Lets start our parsing...
	cur = cur->xmlChildrenNode;
	while (cur)
	{
		// Check if we found a table...
		if (!xmlStrcmp(cur->name, (const xmlChar *) "table"))
		{
			tableName = (char *) xmlGetProp(cur, (const xmlChar *) "name");
			
			// Check we have a table name...
			if (tableName)
			{
				// Find table
				tmpTable = lookupTable(fwHash,tableName);
				chainHash = tmpTable->chains;
		
				// Process our table list...
				tableNode = cur->xmlChildrenNode;
				while (tableNode)
				{
				
					// Check if we are in the chain item
					if (!xmlStrcmp(tableNode->name, (const xmlChar *) "chain"))
					{
						tmpProp = getProperties(tableNode);
						chainName = g_hash_table_lookup(tmpProp,"name");
						
						// Check we have a chain name... this we must
						if (chainName)
						{
							// Find chain
							tmpChain = lookupChain(chainHash,chainName);
							tmpChain->defaultTarget = g_hash_table_lookup(tmpProp,"default");
							ruleList = tmpChain->ruleList;
	
							// Process our chain list
							chainNode = tableNode->xmlChildrenNode;
							while (chainNode)
							{
								// Check we are in our rule...
								if (!xmlStrcmp(chainNode->name, (const xmlChar *) "rule"))
								{
									tmpProp = getProperties(chainNode);
									ruleName = g_hash_table_lookup(tmpProp,"name");
									contentList = splitContents(chainNode);

									tmpRule = (struct confACLRule_t*) malloc(sizeof(struct confACLRule_t));
									tmpRule->target = g_hash_table_lookup(tmpProp,"target");
									tmpRule->classList = contentList;
									tmpRule->params = tmpProp;

									// Check if everything is ok
									if (tmpRule->target == NULL)
									{
										fprintf(stderr,"ERROR: All rules MUST have a target!\n");
										free(tmpRule);
									}
									else
									{
										// Build our ruleset
										tmpChain->ruleset = g_list_concat(tmpChain->ruleset,
													createRuleset(classHash,tmpRule->classList,
													chainName,tmpRule->target,
													g_hash_table_lookup(tmpProp,"cmd-line"),
													NULL)
												);
										
										ruleList = g_list_append(ruleList,tmpRule);
									}
								}
								// Advance chain node
								chainNode = chainNode->next;
							}
						}
					}
					// Advance table	
					tableNode = tableNode->next;
				}
			}
			else
				fprintf(stderr,"ERROR: Table found without a name\n");
		}
		// Advance our acl node
		cur = cur->next;
	}

	return(fwHash);
}



