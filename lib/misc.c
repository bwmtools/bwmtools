/*
 * 	misc.c - Misc functions we can use
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  01/09/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>
#include <common.h>


// Convert everything between start & end into an integer
int aptrtoi(char *start, char *end)
{
	char *str = alloca(1024);

	if (!start)
		return 0;

	if (end)
		strncpy(str,start,end - start);
	else
		strcpy(str,start);

	return atoi(str);
}


// Convert datetime format into unix time format
int parseDateTime(char *dateTime)
{
	int day = 0, month = 0, year = 0, hour = 0, minute = 0, second = 0;
	struct tm time_p;
	int ret;
       

	// Scan datetime provided and get our variables
	ret = sscanf(dateTime, "%d/%d/%d %d:%d:%d", &year, &month, &day, &hour, 
			&minute, &second);

	// Check if we have an acceptable number of variables scanned
	if ( ret == 3 || ret == 5 || ret == 6 )
	{
		time_p.tm_mday = day;
		time_p.tm_mon = month - 1;  // Months since Jan
		time_p.tm_year = year - 1900;  // Years since 1900
		time_p.tm_hour = hour;
		time_p.tm_min = minute;
		time_p.tm_sec = second;
		ret = mktime(&time_p);
	}
	else
		ret = -1;

	// If we encountered an error.. complain
	if (ret == -1)
		fprintf(stderr,"ERROR: Failed to convert %s to date time",dateTime);
	
	return ret;
}


// Format a counter which is in long long int into a nice looking string value
char *counterFMT(char *unit, double value)
{
	char *buffer;
	double myValue = value;
	int level = 0;
	char *prefix;


	prefix = alloca(1);
	
	// Down our value by dividing by 1024 till we below 1000 or we hit Giga	
	while (myValue > 1000 && level <= 4)
	{
		level++;
		myValue /= 1024;
	}

	// Check what level we at
	switch (level)
	{
		case 0:
			prefix = "";
			break;
		case 1: 
			prefix = "K"; 
			break;
		case 2:
			prefix = "M";
			break;
		case 3:
			prefix = "G";
			break;
		case 4:
			prefix = "T";
			break;
	}

	// Write to buffer and return
	buffer = (char *) malloc0(BUFFER_SIZE);
	snprintf(buffer,BUFFER_SIZE,"%.2f %s%s",myValue,prefix,unit);

	return(buffer);
}


// Function to send all data to a socket and not just chunks
int sendall(int sock, void *buf, int len)
{
	int total = 0;        // how many bytes we've sent
	int bytesleft = len; // how many we have left to send
	int n;


	while(total < len) 
	{
		n = send(sock, buf + total, bytesleft, MSG_NOSIGNAL);
		if (n == -1) 
			break;
		total += n;
		bytesleft -= n;
	}

	return n == -1 ? -1 : 0; // return -1 on failure, 0 on success
} 


