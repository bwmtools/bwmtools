/*
 * 	display.h - Curses stuff for BWM Monitor header file
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  29/05/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/


#ifndef _IDMS_CURSES_H
#define _IDMS_CURSES_H

#define setTextAttr(window,mask) wbkgdset(window,WA_NORMAL ^ (mask));


// Function to title a window with text
void windowTitle(WINDOW *win, int color1, int color2, int y, int x, char *str);
// Create a panel with specific attribs
WINDOW *mkwindow(int color, int rows, int cols, int tly, int tlx);
// Create a panel with specific attribs
PANEL *mkpanel(int color, int rows, int cols, int tly, int tlx);
// Function to fill a window
void fill_window(WINDOW *win);
// Function to fill a panel
void fill_panel(PANEL *pan);
// Nice small menu driver
int menu_virtualize(int c);

bool outs(char *s);
// Function to clean ncurses stuff up
void cleanup();

	
#endif

