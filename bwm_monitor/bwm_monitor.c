/*
 * 	bwm_monitor.c - BWM Monitor
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  03/06/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/

#include <arpa/inet.h>
#include <curses.h>
#include <errno.h>
#include <panel.h>
#include <menu.h>
#include <netinet/in.h>
#include <assert.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <term.h>
#include <time.h>
#include <unistd.h>
#include "../config.h"
#include "display.h"
#include "flow.h"
#include "flowControl.h"
#include "misc.h"


#define PORT 9034



static void finish(int sig);


void updateWindow(WINDOW *win, int serverSock, char *flowName)
{
        fd_set master_fds;  // Master file descriptor list
        fd_set read_fds;  // Temp file descriptor list for select()
	int fdmax;
	struct ipc_packet_t myPacket;


	myPacket.pktType = IPC_PACKET_ADD;
	strcpy(myPacket.u.statusData.flowName,flowName);
	sendall(serverSock, &myPacket, sizeof(struct ipc_packet_t));
			
	// Zero the sets
	FD_ZERO(&master_fds);
	FD_ZERO(&read_fds);

	// Add the socket to the master set
	FD_SET(serverSock, &master_fds);

	// Set max file descriptor we watching
	fdmax = serverSock;
	
	// We don't want a delay when we try read a character
	nodelay(win,TRUE);

	// Update until we get broken	
	for (;;)
	{
		struct timeval timeout;
		int result;


		// Set our timeout to half a second
		timeout.tv_sec = 0;
		timeout.tv_usec = 250000;

		read_fds = master_fds;
		
            	result = select(fdmax + 1, &read_fds, NULL, NULL, &timeout);

		// Check select() result
		if (result > 0)
		{
			int nbytes;
		

			// Grab packet	
                        if ((nbytes = recv(serverSock, &myPacket, sizeof(struct ipc_packet_t), 0)) <= 0)
			{
				fprintf(stderr,"Failed to receive packet: %s\n",strerror(errno));
				break;
			}

			// Check its the right type
			if (myPacket.pktType != IPC_PACKET_FLOW_DATA)
			{
				fprintf(stderr,"Received incorrect packet\n");
				break;
			}	

		
			// And display everything on screen	
			setTextAttr(win,COLOR_PAIR(5));
			mvwprintw(win, 4, 14, "%11i",myPacket.u.statusData.maxRate);
			mvwprintw(win, 5, 14, "%11i",myPacket.u.statusData.maxQueueLen);
			mvwprintw(win, 4, 40, "%11i",myPacket.u.statusData.burstRate);
			mvwprintw(win, 5, 40, "%11i",myPacket.u.statusData.maxQueueSize);
			setTextAttr(win,COLOR_PAIR(6));
			mvwprintw(win, 9, 14, "%11i",myPacket.u.statusData.running.pktSize);
			mvwprintw(win, 10, 14, "%11i",myPacket.u.statusData.running.pktCount);
			mvwprintw(win, 11, 14, "%10.2f%%",myPacket.u.statusData.queueItemUsage);
			mvwprintw(win, 12, 14, "%11i",myPacket.u.statusData.running.pktBursted);
			mvwprintw(win, 9, 40, "%11i",myPacket.u.statusData.avg.pktSize);
			mvwprintw(win, 10, 40, "%11i",myPacket.u.statusData.avg.pktCount);
			mvwprintw(win, 11, 40, "%10.2f%%",myPacket.u.statusData.queueSizeUsage);
			mvwprintw(win, 12, 40, "%11i",myPacket.u.statusData.running.pktDropped);

			wrefresh(win);
		}
		else if (result < 0)
		{ 	// error occured
		  	// FIXME - set error message
			break;
		}
		
		// Check if we got a keypress
		if (wgetch(win) != ERR)
		{
			// If we did, remove the flow we monitoring from our list
			myPacket.pktType = IPC_PACKET_DEL;
			strcpy(myPacket.u.statusData.flowName,flowName);
			sendall(serverSock, &myPacket, sizeof(struct ipc_packet_t));
			break;
		}
	}

	// Free the memory we allocated
	//free(stats);
}


void statusWindow(int x, int y, int serverSock, char *flowName)
{
	WINDOW *myWindow;
	char *buffer;


	buffer = malloc(1024);

	// Create our window ready for updating
	myWindow = mkwindow(4,17,55,x,y);
	fill_window(myWindow);

	// Setup title	
	snprintf(buffer,1024,"Traffic Flow: %s",flowName);
	windowTitle(myWindow,2,4,0,2,buffer);

	setTextAttr(myWindow,COLOR_PAIR(4) | WA_UNDERLINE | WA_BOLD);
	mvwprintw(myWindow, 2, 2, "Limits:");
	setTextAttr(myWindow,COLOR_PAIR(4));
	mvwprintw(myWindow, 4, 3, "Max Rate :");
	mvwprintw(myWindow, 4, 27, "Burst Rate:");
	mvwprintw(myWindow, 5, 3, "Queue Len:");
	mvwprintw(myWindow, 5, 27, "Queue Size:");

	setTextAttr(myWindow,COLOR_PAIR(4) | WA_UNDERLINE | WA_BOLD);
	mvwprintw(myWindow, 7, 2, "Current:");
	setTextAttr(myWindow,COLOR_PAIR(4));
	mvwprintw(myWindow, 9, 3, "Rate     :");
	mvwprintw(myWindow, 9, 27, "Rate Avg  :");
	mvwprintw(myWindow, 10, 3, "Pkt/s    :");
	mvwprintw(myWindow, 10, 27, "Pkt/s Avg :");
	mvwprintw(myWindow, 11, 3, "Queue Len:");
	mvwprintw(myWindow, 11, 27, "Queue Size:");
	mvwprintw(myWindow, 12, 3, "Bursts   :");
	mvwprintw(myWindow, 12, 27, "Drops     :");
	
	mvwprintw(myWindow, 15, 16, "Press any key to quit...");
	
	// Refresh the screen	
	wrefresh(myWindow);

	// Keep it up to date
	updateWindow(myWindow,serverSock,flowName);
	
	// Erase window
	wbkgdset(myWindow, COLOR_PAIR(7));
	werase(myWindow);
	wrefresh(myWindow);
	delwin(myWindow);

	free(buffer);
}




int main(int argc, char *argv[])
{
	WINDOW *w_main;
	MENU *m_main;
	WINDOW *w_menu = NULL;
	ITEM **menu_items;
	int mrows, mcols;
	int c;
	char *menuItem;
	int done = 0;
	int numFlows;
	int mySock;
        struct sockaddr_in serverAddr;  // Server address
	struct ipc_packet_t myPacket;
	int nbytes;
	

	signal(SIGINT, finish);

	// Get us a socket
	if ((mySock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		fprintf(stderr,"Failed to get socket: %s\n",strerror(errno));
		exit(1);
	}

	// Connect
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	memset(&(serverAddr.sin_zero), '\0', 8);
	if (connect(mySock,(struct sockaddr *) &serverAddr, sizeof(struct sockaddr)) == -1) 
	{
		fprintf(stderr,"Failed to connect to server: %s\n",strerror(errno));
		exit(1);
	}

	// Send a NUM_FLOWS packet to find out how many flows we have
	myPacket.pktType = IPC_PACKET_GET_NUM_FLOWS;
	time(&myPacket.u.pingData.timestamp);

	if (sendall(mySock,&myPacket,sizeof(struct ipc_packet_t)) != 0)
	{
		fprintf(stderr,"bwm_monitor: Failed to write to socket: %s\n",strerror(errno));
		exit(1);
	}

	// Check that we got a reply	
	if ((nbytes = recv(mySock, &myPacket, sizeof(struct ipc_packet_t), 0)) <= 0) 
	{
		// Got error or connection closed by client
		if (nbytes == 0) 
		{
			// connection closed
			fprintf(stderr,"bwm_monitor: Socket %d fucked up\n", mySock);
		}
		else
		{
			fprintf(stderr,"bwm_monitor: Error receiving packet: %s\n",strerror(errno));
		}
		close(mySock);
		exit(1);
	}      
	
	// Check the reply is a PONG
	if (myPacket.pktType != IPC_PACKET_NUM_FLOWS)
	{
		fprintf(stderr,"bwm_monitor: Received incorrect packet in reply to GET_NUM_FLOWS\n");
		exit(1);
	}
	numFlows = myPacket.u.num;

	printf("Connection to bwmd open: %i flows\n",numFlows);

	// Initialize curses...
	w_main = initscr();
	keypad(stdscr, TRUE);  /* enable keyboard mapping */
//	nonl(); /* tell curses not to do NL->CR/NL on output */
	noraw();
	cbreak();  /* take input chars one at a time, no wait for \n */
	noecho(); 
	outs(cursor_invisible);


	// Init our colors
	if (has_colors())
	{
		start_color();

		init_pair(1, COLOR_BLUE,    COLOR_BLACK);
		init_pair(2, COLOR_WHITE,   COLOR_BLACK);
		init_pair(3, COLOR_GREEN,   COLOR_BLACK);
		init_pair(4, COLOR_WHITE,   COLOR_BLUE);
		init_pair(5, COLOR_YELLOW,   COLOR_BLUE);
		init_pair(6, COLOR_YELLOW,   COLOR_BLACK);
		init_pair(7, COLOR_BLACK,   COLOR_BLACK);
//		init_pair(1, COLOR_RED,     COLOR_BLACK);
//		init_pair(2, COLOR_GREEN,   COLOR_BLUE);
//		init_pair(3, COLOR_YELLOW,  COLOR_BLACK);
//		init_pair(5, COLOR_CYAN,    COLOR_BLACK);
//		init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
		
	}

	// Set our main screen
	wbkgdset(w_main, COLOR_PAIR(2) | ' ');
	fill_window(w_main);
	windowTitle(w_main,3,2,0,2,"BWM Monitor v"PACKAGE_VERSION" - Copyright (c) 2003-2006 Linux Based Systems Design");	
	refresh();



	// Send a LIST packet away to get a flow list
	myPacket.pktType = IPC_PACKET_LIST_FLOWS;

	if (sendall(mySock,&myPacket,sizeof(struct ipc_packet_t)) != 0)
	{
		fprintf(stderr,"bwm_monitor: Failed to write to socket: %s\n",strerror(errno));
		exit(1);
	}
	
	c = 0;
	// Allocate memory for menu items
	menu_items = (ITEM **) malloc((numFlows + 1) * sizeof(ITEM *));
	do	
	{
		// Grab pavket
		if (recv(mySock, &myPacket, sizeof(struct ipc_packet_t), 0) > 0) 
		{
			// Check type
			if (myPacket.pktType == IPC_PACKET_LIST_ITEM)
			{
				// And add memory item
				menu_items[c] = new_item(strdup(myPacket.u.statusData.flowName),"");	
				c++;		
			}
		}
	} while (myPacket.pktType == IPC_PACKET_LIST_ITEM);

	

	// Don't forget our quit item
	menu_items[numFlows] = new_item(strdup("Quit"),"");
	menu_items[numFlows+1] = NULL;
	m_main = new_menu(menu_items);
	set_menu_format(m_main, (numFlows / 2) + 2, 1);
	// Get size of min window for menu, redefine menu window
	scale_menu(m_main,&mrows,&mcols);
	// Loop in our menu
	while (!done)
	{
		setTextAttr(w_menu,WA_BOLD);
		// Create window for menu
		w_menu = mkwindow(4, mrows + 2, mcols + 2, 2, 2);
		// Make it look nice
		fill_window(w_menu);
		set_menu_win(m_main,w_menu);
		keypad(w_menu,TRUE);
		set_menu_sub(m_main,derwin(w_menu,mrows,mcols,1,1));
		post_menu(m_main);
		wrefresh(w_menu);

		// Take input on our menu
		while ((c = menu_driver(m_main, menu_virtualize(wgetch(w_menu)))) != E_UNKNOWN_COMMAND) 
		{
			if (c == E_REQUEST_DENIED)
				beep();
			continue;
		}

		menuItem = (char *) item_name(current_item(m_main));
		// Check if we must quit
		if (!strcasecmp(menuItem,"quit"))
			done = 1;
		else
			statusWindow(5, mcols + 10, mySock, menuItem);

		// Remove our menu
		unpost_menu(m_main);
		wbkgdset(w_menu, COLOR_PAIR(7));
		werase(w_menu);
		wrefresh(w_menu);
		delwin(w_menu);

	}


	// Free menu items
	for (c = 0; c < numFlows + 1; c++)
		free_item(menu_items[c]);
//	free(menu_items);
	free_menu(m_main);

	shutdown(mySock,SHUT_RDWR);
	close(mySock);
	
	finish(0);               /* we're done */
	return(0);
}


static void finish(int sig)
{
	endwin();
		
	cleanup();
	
	exit(0);
}
