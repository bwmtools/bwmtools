/*
 * 	display.c - Curses stuff for BWM Monitor
 * 	Copyright (C) 2003-2006, Linux Based Systems Design
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2 of the License, or
 * 	(at your option) any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 * 	
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program; if not, write to the Free Software
 * 	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *  29/05/2003 - Nigel Kukard  <nkukard@lbsd.net>
 *      * Initial design
*/

#include <curses.h>
#include <panel.h>
#include <menu.h>
#include <string.h>
#include <unistd.h>
#include <term.h>

#define valid(s) ((s != 0) && s != (char *)-1)

// Function to title a window with text
void windowTitle(WINDOW *win, int color1, int color2, int y, int x, char *str)
{
	int len;

	len = strlen(str);
	mvwaddch(win, y, x, ACS_RTEE);
	wattrset(win,COLOR_PAIR(color1));
	mvwprintw(win, y, x + 1, " %s ", str);
	wstandend(win);
	wattrset(win,COLOR_PAIR(color2));
	mvwaddch(win, y, x + 3 + len, ACS_LTEE);
}


// Create a panel with specific attribs
WINDOW *mkwindow(int color, int rows, int cols, int tly, int tlx)
{
	WINDOW *win;

	if ((win = newwin(rows, cols, tly, tlx)) != 0) 
	{
		if (has_colors()) 
		{
	    		int fg = (color == COLOR_BLUE) ? COLOR_WHITE : COLOR_BLACK;
	    		int bg = color;
	    
			init_pair(color, fg, bg);
			wbkgdset(win, COLOR_PAIR(color) | ' ');
		} 
		else 
		{
			wbkgdset(win, A_BOLD | ' ');
		}
	
	}

	return win;
}



// Create a panel with specific attribs
PANEL *mkpanel(int color, int rows, int cols, int tly, int tlx)
{
	WINDOW *win;
    	PANEL *pan = 0;

	if ((win = newwin(rows, cols, tly, tlx)) != 0) 
	{
		if ((pan = new_panel(win)) == 0) 
		{
	    		delwin(win);
		} 
		else 
			if (has_colors()) 
			{
		    		int fg = (color == COLOR_BLUE) ? COLOR_WHITE : COLOR_BLACK;
		    		int bg = color;
		    
				init_pair(color, fg, bg);
				wbkgdset(win, COLOR_PAIR(color) | ' ');
			} 
			else 
			{
				wbkgdset(win, A_BOLD | ' ');
			}
	
	}
	return pan;
}

// Function to fill a window
void fill_window(WINDOW *win)
{
    	int y, x;

    	wclrtoeol(win);
    	box(win, 0, 0);
    	for (y = 1; y < getmaxy(win) - 1; y++) 
	{
		for (x = 1; x < getmaxx(win) - 1; x++) 
		{
			wmove(win, y, x);
			waddch(win, ' ');
		}
	}
}

// Function to fill a panel
void fill_panel(PANEL * pan)
{
	WINDOW *win = panel_window(pan);

	fill_window(win);
}

// Nice small menu driver
int menu_virtualize(int c)
{
    if (c == '\n' || c == KEY_EXIT)
	return (MAX_COMMAND + 1);
    else if (c == 'u')
	return (REQ_SCR_ULINE);
    else if (c == 'd')
	return (REQ_SCR_DLINE);
    else if (c == 'b' || c == KEY_NPAGE)
	return (REQ_SCR_UPAGE);
    else if (c == 'f' || c == KEY_PPAGE)
	return (REQ_SCR_DPAGE);
    else if (c == 'n' || c == KEY_DOWN)
	return (REQ_NEXT_ITEM);
    else if (c == 'p' || c == KEY_UP)
	return (REQ_PREV_ITEM);
    else if (c == ' ')
	return (REQ_TOGGLE_ITEM);
    else {
	if (c != KEY_MOUSE)
	    beep();
	return (c);
    }
}


// Fucntion to write a character to stdout
int outc(int c)
{
	char tmp = c;
	write(STDOUT_FILENO, &tmp, 1);
	return 0;
}

// function to write a string to stdout
bool outs(char *s)
{
	if (valid(s)) 
	{
		tputs(s, 1, outc);
		return TRUE;
	}
	return FALSE;
}

// function to clean stuff up
void cleanup()
{
	outs(exit_attribute_mode);
	if (!outs(orig_colors))
		outs(orig_pair);
	outs(clear_screen);
	outs(cursor_normal);
}


